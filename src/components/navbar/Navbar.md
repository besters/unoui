#### Basic empty Navbar
```jsx
<Navbar />
```

#### Navbar with text brand
```jsx
<Navbar brandTitle="UnoUI" />
```

#### Navbar with logo
```jsx
<Navbar brandLogo="http://0.0.0.0:6060/public/logo-dark.png" />
```

#### Customized brand
```jsx
<Navbar
  brandLogo="http://0.0.0.0:6060/public/logo-dark.png"
  brandTitle="UnoUI"
  brandTarget="/admin"
  brandColor="white-10" />
```

### Navbar style
```jsx
<Navbar
  color="deep-purple"
  brandTitle="UnoUI"
  brandColor="deep-purple-60"
  component
  flat
  />
```

### Left elements
```jsx
<Navbar
  color="black-60"
  brandLogo="http://0.0.0.0:6060/public/logo.png"
  brandColor="black-90"
  leftEl={<div className="f6 flex items-center">
            <FaGithub size={20} className="mr1" />
            <span>Some text or <a href="#">link</a> and icon</span>
          </div>}
  component
  />
```

### Right elements
```jsx
<Navbar
  brandLogo="http://0.0.0.0:6060/public/logo-dark.png"
  rightEl={<div className="f6 flex items-center">
              <FaGithub size={20} className="mr1" />
              <span>Some text or <a href="#">link</a> and icon</span>
            </div>}
  component
  flat
  />
```

### Left and right element
```jsx
<Navbar
  color="white-20"
  brandLogo="http://0.0.0.0:6060/public/logo-dark.png"
  rightEl={<div className="f6">@TODO: Dropdown</div>}
  leftEl={<div className="f6 flex items-center">
          <FaCogs size={20} className="mr2" />
          <span><strong>Any Component</strong> can be here</span>
        </div>}
  component
  flat
  />
```

### Multiple navbars
```jsx
<div>
  <Navbar color="teal" brandLogo="http://0.0.0.0:6060/public/logo.png" />
  <Navbar color="teal-90" leftEl={<div>Brand is not required</div>} />
</div>
```