// @flow

import React from 'react'
import cx from 'classnames'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import type { Node } from 'react'

type PropTypes = {
  /** Background color */
  color?: string,

  /** Flat navbar has no shadow */
  flat?: boolean,

  /** Company or product logo. */
  brandLogo?: string,

  /** Title for brand, usually company or product name. */
  brandTitle?: string,

  /** Background color of brand */
  brandColor?: string,

  /** Href for brand element */
  brandTarget?: string,

  /** Render minified */
  brandMini?: boolean,

  /** Do not render */
  brandHidden?: boolean,

  /** ID of the main sidebar. Sync brand appearance with sidebar. */
  mainNavId?: string,

  /** Render as standalone component in the middle of a page (adds rounded corners basically) */
  component?: boolean,

  /** Custom component on left side, usually dropdown or navigation */
  leftEl?: Node,

  /** Custom component on right side, usually dropdown or navigation */
  rightEl?: Node,

  /** Custom CSS classes */
  className?: string,
}

const _getBgColor = (color?: string): string => {
  if (!color) return ''

  return `bg-${color}`
}

const wrapperClasses = (props: PropTypes) =>
  cx('h3 ph3 flex items-stretch', _getBgColor(props.color), props.className, {
    'shadow-1': !props.flat,
    'b--white-30 ba': props.flat && props.color == 'white', // Special border when color is white and the navbar is flat
    br2: props.component,
  })

const _renderLeftEl = (el: Node) => (
  <div className="flex items-center ml3 z-1 relative">{el}</div>
)

const _renderRightEl = (el: Node) => (
  <div className="ml-auto flex items-center z-1 relative">{el}</div>
)

const __brandClasses = (props: PropTypes) =>
  cx(
    'nl3 pr4 flex items-center z-1 overflow-hidden',
    _getBgColor(props.brandColor),
    {
      'br--left br2': props.component,
      w6: !props.brandMini,
      w3: props.brandMini,
    }
  )

const _brand = (props: PropTypes) => (
  <div className={__brandClasses(props)}>
    <Link
      to={props.brandTarget}
      className="pv3 ph4 no-underline underline-hover f5"
      style={{ color: 'currentColor' }}>
      {props.brandLogo ? (
        <img
          src={props.brandLogo}
          alt={props.brandTitle}
          title={props.brandTitle}
        />
      ) : (
        props.brandTitle
      )}
    </Link>
  </div>
)

const _renderBrand = (props: PropTypes) => {
  if (props.brandHidden) return

  if (props.brandTitle || props.brandLogo) return _brand(props)
}

/**
* In order to display Brand, you need to set brandLogo, or brandTitle prop.
*/
const Navbar = (props: PropTypes) => (
  <div className={wrapperClasses(props)}>
    {_renderBrand(props)}
    {props.leftEl && _renderLeftEl(props.leftEl)}
    {props.rightEl && _renderRightEl(props.rightEl)}
  </div>
)

Navbar.defaultProps = {
  color: 'white',
  flat: false,
  component: false,
  brandTarget: '/',
  brandMini: false,
  brandHidden: false,
}

const mapStateToProps = (state, ownProps) => {
  const { mainNavId } = ownProps

  return {
    brandMini: state.unoui.getIn(['sidebars', mainNavId, 'mini']),
    brandHidden: state.unoui.getIn(['sidebars', mainNavId, 'hide']),
  }
}

export default connect(mapStateToProps, null)(Navbar)
