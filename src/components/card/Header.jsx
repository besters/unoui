// @flow

import React from 'react'
import type { Node } from 'react'
import cx from 'classnames'

import { _bg, _fs } from '../../helpers/class-names.jsx'

import './header.scss'

type PropTypes = {
  children: Node,
  subtitle?: string,
  subtitleInline?: boolean,
  headingElement?: Node,
  leftElement?: Node,
  rightElement?: Node,
  background?: string,
  flat?: boolean,
  headerElement?: Node,
  className?: string,
  titleSize?: number,
}

// When custom bg color is set
const _bgColorCss = (background?: string): string => {
  if (!background) return ''

  return background == 'white'
    ? 'b--white-30 bg-white' // When color is white
    : `${_bg(background)} card__header--transparent-border` // Any other colors but white
}

// Flat is true
const _flatCss = (background?: string): string =>
  cx('pt3 bb-0', {
    'bg-white': !background, // No custom bg color
  })

// Flat is false
const _noFlatCss = (background?: string): string =>
  cx('pv3', {
    'b--white-30 bg-white-05': !background, // No custom bg color
  })

const getWrapperClasses = (props: PropTypes) =>
  cx('br2 br--top ph3 flex justify-between items-center bb', props.className, {
    [_bgColorCss(props.background)]: props.background, // Custom background color is set
    [_flatCss(props.background)]: props.flat,
    [_noFlatCss(props.background)]: !props.flat,
  })

const renderSubtitle = (subtitle?: string, inline: ?boolean) => {
  if (!subtitle) return null

  const classes = cx('f6 fw1', {
    'db mt1 ml2': !inline,
    ml2: inline,
  })

  return <small className={classes}>{subtitle}</small>
}

const renderHeadingElement = (element?: Node) => {
  if (!element) return null

  return <div className="flex items-center">{element}</div>
}

const renderLeftElement = (element?: Node) => {
  if (!element) return null

  return <div className="mr2">{element}</div>
}

const renderRightElement = (element?: Node) => {
  if (!element) return null

  return <div className="ml2">{element}</div>
}

const renderTitleContent = (props: PropTypes) => (
  <div className={_fs(props.titleSize)}>{props.children}</div>
)

const renderTitle = (props: PropTypes) => {
  const classes = cx('pv2 fw5', {
    'flex items-baseline': props.subtitleInline, // Show inline if subtitle is set to inline
  })

  return (
    <h5 className={classes}>
      <div className="flex items-center pl2">
        {renderLeftElement(props.leftElement)}
        {renderTitleContent(props)}
        {renderRightElement(props.rightElement)}
      </div>
      {renderSubtitle(props.subtitle, props.subtitleInline)}
    </h5>
  )
}

const CardHeader = (props: PropTypes) => {
  return (
    <div className={getWrapperClasses(props)}>
      {renderTitle(props)}
      {renderHeadingElement(props.headerElement)}
    </div>
  )
}

CardHeader.defaultProps = {
  subtitleInline: false,
  flat: false,
  titleSize: 5,
}

export default CardHeader
