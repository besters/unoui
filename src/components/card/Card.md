### Basic Card heading options

```jsx
<Card>Default card witout any props</Card>
```

```jsx
<Card title="Card title">Basic card</Card>
```

```jsx
<Card title="Card title" subtitle="With block subtitle">Basic card</Card>
```

```jsx
<Card title="Card title" subtitle="With inline subtitle" subtitleInline>Basic card</Card>
```

### Card title posibilities

```jsx
<Card title={<div>Regular <span className="fw1">Thin</span></div>}>Example of custom title markup</Card>
```

```jsx
<Card title={
    <div>
        <span className="ttu i">Uppercase italic </span>
        <span className="b red">Red bold</span>
    </div>
}>Example of custom title markup</Card>
```

```jsx
<Card title={
    <div>
        Word
        <span className="bg-orange pa1">Highlighted</span>
    </div>
}>You can even put icons, badges, or anything you like to the title</Card>
```

#### Complete title customization example
```jsx
<Card
    title={
        <div className="inline-flex items-center">
            <span className="ttu bg-purple pv1 ph2 f7 fw3 mr2">badge</span>
            <IoIosGearOutline size={24} />
            <span className="no-flex">Regular <span className="fw1">Thin</span></span>
            <IoIosGearOutline size={24} className="ml2" />
        </div>
    }
    subtitle="Basic subtitle"
    headerClassName="bg-red-30 green-90"
    >
Obsah karty
</Card>
```


### Card header elements

```jsx
<Card
    title="Left title icon"
    titleLeftElement={<FaCogs size={20} />}
    >Card with left icon</Card>
```

```jsx
<Card
    title="Right title icon"
    titleRightElement={<FaCogs size={20} />}
    >Card with right icon</Card>
```

```jsx
<Card
    title="Heading text"
    headerElement={
        <div className="f6 flex items-center">
            <FaGithub size={20} className="mr1" />
            <span>Some text or <a href="#">link</a> and icon</span>
        </div>
    }
    >Custom heading text. Basically any component can be rendered there</Card>
```

```jsx
<Card
    title="All elements"
    titleLeftElement={<FaCogs size={20} />}
    titleRightElement={<IoIosGearOutline size={20} />}
    headerElement={<FaGithub size={20} />}
    >Card with all elements set</Card>
```

### Card styles

```jsx
<Card title="Default Card">
    Basic card
</Card>
```

```jsx
<Card title="Flat Card" flat>
    Basic flat card
</Card>
```

```jsx
<Card title="Minimal depth" depth={0}>
    Card with depth set to 0
</Card>
```

```jsx
<Card title="Maximal depth" depth={5}>
    Card with depth set to 5
</Card>
```

```jsx
<Card title="Card with background color" background="deep-purple-30">
    Any color from UnoUI color system can be used
</Card>
```

```jsx
<Card title="Card with header background" headerBg="brown-40">
    Any color from UnoUI color system can be used
</Card>
```

```jsx
<Card title="White card" headerBg="white">
    Plain white card
</Card>
```

```jsx
<Card title="Flat card with background color" background="pink" flat>
    Any color from UnoUI color system can be used
</Card>
```

## Headings

```jsx
    <Card title="Header 1" titleSize={1}>
        H1 title heading
    </Card>
```

```jsx
    <Card title="Header 2" titleSize={2}>
        H2 title heading
    </Card>
```

```jsx
    <Card title="Header 3" titleSize={3}>
        H3 title heading
    </Card>
```

```jsx
    <Card title="Header 4" titleSize={4}>
        H4 title heading
    </Card>
```

```jsx
    <Card title="Header 5 (default)" titleSize={5}>
        H5 title heading (default)
    </Card>
```

```jsx
    <Card title="Header 6" titleSize={6}>
        H6 title heading
    </Card>
```

```jsx
    <Card title="Header 7" titleSize={7}>
        H7 title heading
    </Card>
```

### Custom CSS classes examples

```jsx
<Card title="Hover me" flat className="shadow-hover-5 grow">
    Hover me
</Card>
```

```jsx
<Card title="Hover me" depth={5} className="shadow-hover-2">
    Hover me
</Card>
```

```jsx
<Card title="Custom card border" flat className="b--red ba">
    Bordered card
</Card>
```

```jsx
<Card title="Left custom card border" flat className="b--green bl bw1">
    Bordered card
</Card>
```

```jsx
<Card title="Top custom card border" flat className="b--amber bt bw2">
    Bordered card
</Card>
```
