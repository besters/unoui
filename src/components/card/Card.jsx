// @flow

import React from 'react'
import cx from 'classnames'

import type { Node } from 'react'

import CardHeader from './Header.jsx'

type PropTypes = {
  /** Card content */
  children: Node,

  /** Card title. Can be string, or any JSX element */
  title?: Node,

  /** Size of the card header. Number between 1 to 7 */
  titleSize?: number,

  /** Optional subtitle under (next to) the title */
  subtitle?: string,

  /** Wether render subtitle on the same line, or next line. */
  subtitleInline?: boolean,

  /** Shadow size. A number between 0 to 5 */
  depth?: number,

  /** Background color for the whole card. */
  background?: string,

  /** Background color for header */
  headerBg?: string,

  /** Flat style for card. Content is closer to header and header has no bottom border. */
  flat?: boolean,

  /** Additional card css classes */
  className?: string,

  /** Additional header css classes */
  headerClassName?: string,

  /** JSX element on the left side of the title */
  titleLeftElement?: Node,

  /** JSX element on the right side of the title */
  titleRightElement?: Node,

  /** JSX element on the right side of the header */
  headerElement?: Node,
}

const _getShadowDepth = (depth?: number): string => {
  if (!depth || isNaN(depth) || depth === 0 || depth > 5) {
    return 'ba bw05 b--white-20'
  } else {
    return `shadow-${depth}`
  }
}

const _getBgColor = (background?: string): string =>
  background ? `bg-${background}` : 'bg-white'

const cardCss = (props: PropTypes): string =>
  cx(
    'br2',
    _getBgColor(props.background),
    _getShadowDepth(props.depth),
    props.className
  )

const _renderHeader = (props: PropTypes) => {
  if (!props.title) return null

  return (
    <CardHeader
      titleSize={props.titleSize}
      subtitle={props.subtitle}
      subtitleInline={props.subtitleInline}
      leftElement={props.titleLeftElement}
      rightElement={props.titleRightElement}
      headerElement={props.headerElement}
      background={props.headerBg || props.background}
      className={props.headerClassName}
      flat={props.flat}>
      {props.title}
    </CardHeader>
  )
}

const _renderBody = (props: PropTypes) => (
  <div className="pa3">
    <div className="pa2 f5">{props.children}</div>
  </div>
)

/**
* Panel s obsahem.
* Je to ten nejzákladnější stavební prvek stránky
*/
const Card = (props: PropTypes) => (
  <div className={cardCss(props)}>
    {_renderHeader(props)}
    {_renderBody(props)}
  </div>
)

Card.defaultProps = {
  subtitleInline: false,
  flat: false,
  depth: 1,
  titleSize: 5,
}

export default Card
