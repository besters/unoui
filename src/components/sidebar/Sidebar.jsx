// @flow

import React from 'react'
import cx from 'classnames'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { startsWith } from 'lodash'

import { initialize } from './actions.jsx'

import type { Node } from 'react'

type Props = {
  children: Node,

  /** Unique identifier to use with collapse and hide buttons */
  id?: string,

  /** Thin sidebar. Usually used as a collapsed navigation */
  mini?: boolean,

  /** When true, sidebar is not rendered at all */
  hide?: boolean,

  /** For use inside page content. */
  detached?: boolean,

  /** Background color */
  color?: string,

  /** Function to registering this sidebar in the Redux store */
  initialize: (data: Object) => void,

  className?: string,
}

const defaultCSS = 'w6'
const miniCSS = 'w3'
const detachedCSS = 'br2'

const _getBgColor = (background?: string): string => {
  const isCustomColor = startsWith(background, '#')
  if (isCustomColor) return ''

  return background ? `bg-${background}` : ''
}

const classes = (props: Props) =>
  cx(_getBgColor(props.color), props.className, 'shadow-1', 'z-1', {
    [defaultCSS]: !props.mini,
    [miniCSS]: props.mini,
    [detachedCSS]: props.detached,
  })

const _style = (props: Props) => {
  const isCustomColor = startsWith(props.color, '#')
  if (!isCustomColor) return {}

  return {
    backgroundColor: props.color,
  }
}

const _renderSidebar = (props: Props) => (
  <div className="flex">
    <div className={classes(props)} style={_style(props)}>
      <div className="z-2 relative overflow-hidden">{props.children}</div>
    </div>
  </div>
)

const _renderDetached = (props: Props) => (
  <div className="mh3 flex">{_renderSidebar(props)}</div>
)

/**
* Vertical area that displays onscreen and presents widget components and website navigation menu in a text-based hierarchical form.
*/
class Sidebar extends React.PureComponent<Props> {
  static defaultProps = {
    mini: false,
    detached: false,
    color: 'white',
    hide: false,
    primary: false,
  }

  componentDidMount() {
    if (!this.props.id) return

    // Register this sidebar in Redux
    this.props.initialize({
      id: this.props.id,
      mini: this.props.mini,
      hide: this.props.hide,
    })
  }

  render() {
    if (this.props.hide) return null

    return this.props.detached
      ? _renderDetached(this.props)
      : _renderSidebar(this.props)
  }
}

const mapStateToProps = (state, ownProps) => {
  const { id, mini, hide } = ownProps

  return {
    mini: state.unoui.getIn(['sidebars', id, 'mini']) || mini,
    hide: state.unoui.getIn(['sidebars', id, 'hide']) || hide,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    initialize: param => {
      dispatch(initialize(param))
    },
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar))
