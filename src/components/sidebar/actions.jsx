import { createAction } from 'redux-actions'

export const initialize = createAction('UNOUI/SIDEBAR/INITIALIZE')
export const toggleHide = createAction('UNOUI/SIDEBAR/TOGGLE_HIDE')
export const toggleMinify = createAction('UNOUI/SIDEBAR/TOGGLE_MINIFY')

// const actionCreators = createActions({
//   UNOUI: {
//     SIDEBAR: {
//       INITIALIZE: [
//         amount => ({ amount }),
//         amount => ({ key: 'value', amount }),
//       ],
//       TOGGLE_SHOW: amount => ({ amount: -amount }),
//       TOGGLE_MINIFY: undefined,
//     },
//   },
// })
