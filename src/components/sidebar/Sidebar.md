```jsx
<div className="h5 flex">
  <Sidebar color="slate-90" mini />
</div>
```

### Detached
```jsx
<div className="h5 flex">
  <Sidebar detached />
</div>
```