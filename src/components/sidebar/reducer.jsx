// import typeToReducer from 'type-to-reducer'
import { handleActions } from 'redux-actions'
import Immutable from 'immutable'

import { toggleMinify, toggleHide, initialize } from './actions.jsx'

export const $$initialState = Immutable.fromJS({
  sidebars: {},
})

const _initialize = (state, action) => {
  const { id, mini, hide } = action.payload
  const data = Immutable.Map({ mini: mini, hide: hide })

  return state.setIn(['sidebars', id], data)
}

const _toggleMinify = (state, action) => {
  const id = action.payload

  return state.updateIn(['sidebars', id, 'mini'], val => !val)
}

const _toggleHide = (state, action) => {
  const id = action.payload

  return state.updateIn(['sidebars', id, 'hide'], val => !val)
}

export default handleActions(
  {
    [initialize]: _initialize,
    [toggleMinify]: _toggleMinify,
    [toggleHide]: _toggleHide,
  },
  $$initialState
)
