// @flow

import React from 'react'
import AnimateHeight from 'react-animate-height'
import Color from 'color'
import { withRouter } from 'react-router-dom'
import { matchPath } from 'react-router'
import { includes, without, some, uniq, flattenDeep } from 'lodash'

import Item from './Item.jsx'

import type { ChildrenArray } from 'react'

type Props = {
  items: ChildrenArray<*>,
  isOpen: boolean,
  accordion?: boolean,
  location: Object,
  baseColor: string,
  accentColor?: string,
  isTopLevel?: boolean,
  flat: boolean,
}

type State = {
  activeIndex: Array<number> | number,
}

class Container extends React.PureComponent<Props, State> {
  static defaultProps = {
    isTopLevel: false,
  }

  constructor(props: Props) {
    super()

    // Set default active items
    this.state = { activeIndex: props.accordion ? -1 : [-1] }
  }

  _count = children => React.Children.count(children)

  getAllPaths = () => {
    const reducer = item =>
      this._count(item.props.children) > 0
        ? mapChildren(item.props.children)
        : item.props.path

    const mapChildren = children =>
      React.Children.map(
        children,
        item => (item.type != Item ? '#' : reducer(item))
      )

    let index = 0

    React.Children.forEach(this.props.items, (item: Object) => {
      const { children, exact } = item.props

      if (this._count(children) > 0) {
        const allPaths = uniq(flattenDeep(mapChildren(children)))

        const hasMatch = some(allPaths, (path: string) => {
          return matchPath(this.props.location.pathname, {
            path: path,
            exact: exact,
          })
        })

        if (hasMatch) this.handleToggle(index)
      }

      index += 1
    })
  }

  // We need to loop through all childrens and check if any is active / open
  componentWillMount = () => {
    this.getAllPaths()
  }

  // Check if supplied index is active (opened)
  isIndexActive = (index: number) => {
    const { accordion } = this.props
    const { activeIndex } = this.state
    return accordion ? activeIndex === index : includes(activeIndex, index)
  }

  // Toggle index active / inactive
  handleToggle = (index: number) => {
    const { accordion } = this.props
    const { activeIndex } = this.state

    let newIndex
    if (accordion) {
      newIndex = index === activeIndex ? -1 : index
    } else {
      // check to see if index is in array, and remove it, if not then add it
      newIndex = includes(activeIndex, index)
        ? without(activeIndex, index)
        : [...activeIndex, index]
    }

    this.setState({ activeIndex: newIndex })
  }

  // Return child prop if set
  _childProp = (child, prop) => {
    const childValue = child.props[prop]

    return childValue ? childValue : this.props[prop]
  }

  _cloneChildren = () => {
    let index = 0

    return React.Children.map(this.props.items, (child: Object) => {
      const currentIndex = index

      const isActive = this.isIndexActive(index)

      const toggle = (e: SyntheticEvent<*>) => {
        e.preventDefault()
        this.handleToggle(currentIndex)
      }

      index += 1

      // If it is not an Item, don`t do a shit with it
      if (child.type != Item) return React.cloneElement(child)

      return React.cloneElement(child, {
        submenuIsOpen: isActive,
        toggleSubMenu: toggle,
        accordion: this._childProp(child, 'accordion'),
        accentColor: this._childProp(child, 'accentColor'),
        flat: this._childProp(child, 'flat'),
        baseColor: this.props.baseColor,
        isTopLevel: this.props.isTopLevel,
      })
    })
  }

  _submenuStyle = () => {
    const c = Color(this.props.baseColor)
    const color = c.mix(Color(c.darken(0.8)), 0.6).alpha(0.5)

    if (this.props.isTopLevel) return {}

    return {
      backgroundColor: color,
    }
  }

  render = () => (
    <AnimateHeight duration={300} height={this.props.isOpen ? 'auto' : 0}>
      <ul className="pv2" style={this._submenuStyle()}>
        {this._cloneChildren()}
      </ul>
    </AnimateHeight>
  )
}

export default withRouter(Container)
