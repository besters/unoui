```jsx
<Sidebar>
  <Menu>
    <Item path="/bar" label="Bar" />
    <Divider />
    <Item path="/baz" label="Baz" />
    <Divider />
    <Item label="Bar2">
      <Item path="/bar3" label="Baz2"/>
      <Divider />
      <Item path="/bar4" label="Baz3" />
      <Divider />
      <Item path="/bar5" label="Baz4" />
    </Item>
  </Menu>
</Sidebar>
```
