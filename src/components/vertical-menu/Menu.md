Kompletní dokumentace použitých komponent je v sekcích daných komponent.

### Accordion mode
```jsx
<Sidebar>
  <Menu accordion>
    <Item label="Bar 1">
      <Item path="/bar/baz" label="Baz" />
    </Item>
    <Item label="Bar 2">
      <Item path="/bar/baz2" label="Baz" />
    </Item>
    <Item label="Bar 3">
      <Item path="/bar/baz3" label="Baz" />
    </Item>
  </Menu>
</Sidebar>
```

### Different color
```jsx
<Sidebar>
  <Menu color="deep-purple-90">
    <Item path="/bar" label="Bar" />
    <Item path="/baz" label="Baz" />
  </Menu>
</Sidebar>
```

### Accent color
```jsx
<Sidebar>
  <Menu accentColor="amber">
    <Item path="/bar" label="Bar" />
    <Item path="/baz" label="Baz" />
  </Menu>
</Sidebar>
```

### Flat theme
```jsx
<Sidebar>
  <Menu flat>
    <Item path="/bar" label="Bar" />
    <Item path="/baz" label="Baz" />
  </Menu>
</Sidebar>
```

### Custom class
```jsx
<Sidebar>
  <Menu className="bl bw2 b--l-red">
    <Item path="/bar" label="Bar" />
    <Item path="/baz" label="Baz" />
  </Menu>
</Sidebar>
```
