// @flow

import React from 'react'

/**
* Just a divider between menu items.
* Take no props
*/
const Divider = () => (
  <li className="Menu__light--05 w-100 mv1" style={{ height: '1px' }} />
)

export default Divider
