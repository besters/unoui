// @flow

import React from 'react'
import cx from 'classwrap'
import { Link as RouteLink } from 'react-router-dom'
import { compose, pure, withState, withHandlers } from 'recompose'

import type { Node } from 'react'

type Props = {
  /** Link content */
  children: Node,

  /** Link path */
  path: string,

  /** When true, will only match if the path matches the location.pathname exactly. */
  exact: boolean,

  /** Is this submenu handler */
  hasSubmenu: boolean,

  /** Has open submenu */
  submenuIsOpen: boolean,

  /** Link background color */
  baseColor: string,

  /** Is this item on the top level (not submenu) */
  isTopLevel: boolean,

  /** Is item active / selected? */
  isActive: boolean,

  /** Function that can toggle submenu */
  toggleSubMenu?: (e: SyntheticEvent<*>) => void,

  /** State hnadles */
  handleHover: () => void,

  /** State */
  isHovered: boolean,
}

// Styles used no matter of a depth
const sharedCSS = {
  base: 'Menu__item items-center flex ph3 pv3 no-underline pointer arial', // basic styles that dont change
  idle: '', // when item is not hovered, opened, or active
  hover: '', // item is hovered
  active: '', // item is active
  open: '', // when item has opened submenu
}

// Styles used only on top level items
const topLevelCSS = {
  base: 'hover-white', // basic styles that dont change
  idle: 'Menu__color--dark', // when item is not hovered, opened, or active
  hover: 'Menu__dark--10', // item is hovered
  active: 'white', // item is active
  open: 'white Menu__light--01', // when item has opened submenu
}

// Styles used only on submenu items
const submenuCSS = {
  base: 'hover-white', // basic styles that dont change
  idle: 'Menu__color--dark', // when item is not hovered, opened, or active
  hover: '', // item is hovered
  active: 'white', // item is active
  open: 'white Menu__dark--05', // when item has opened submenu
}

const enhance = compose(
  withState('isHovered', 'toggleHover', false),
  withHandlers({
    handleHover: ({ toggleHover }) => () => toggleHover(hovered => !hovered),
  }),
  pure
)

/**
* Renders actual link
*/
const Link = (props: Props) => {
  // Scope only top level items
  const _topLevelOnly = (cond: boolean) => props.isTopLevel && cond

  // Scope only submenu items
  const _submenuOnly = (cond: boolean) => !props.isTopLevel && cond

  // Check if item has no other state
  // It is needed, because base styles can`t be overwritten by state styles, so
  // idle styles are rendered only when item has no state.
  const _isIdle = () =>
    !props.isHovered && !props.isActive && !props.submenuIsOpen

  const _sharedClasses = () =>
    cx([
      sharedCSS.base,
      {
        [sharedCSS.idle]: _isIdle(),
        [sharedCSS.hover]: props.isHovered,
        [sharedCSS.active]: props.isActive,
        [sharedCSS.open]: props.submenuIsOpen,
      },
    ])

  const _topLevelClasses = () =>
    cx([
      {
        [topLevelCSS.idle]: _topLevelOnly(_isIdle()),
        [topLevelCSS.base]: props.isTopLevel,
        [topLevelCSS.hover]: _topLevelOnly(props.isHovered),
        [topLevelCSS.active]: _topLevelOnly(props.isActive),
        [topLevelCSS.open]: _topLevelOnly(props.submenuIsOpen),
      },
    ])

  const _submenuClasses = () =>
    cx([
      {
        [submenuCSS.idle]: _submenuOnly(_isIdle()),
        [submenuCSS.base]: !props.isTopLevel,
        [submenuCSS.hover]: _submenuOnly(props.isHovered),
        [submenuCSS.active]: _submenuOnly(props.isActive),
        [submenuCSS.open]: _submenuOnly(props.submenuIsOpen),
      },
    ])
  const _className = () =>
    cx([_sharedClasses(), _topLevelClasses(), _submenuClasses()])

  const renderSimpleLink = () => (
    <a className={_className()} onClick={props.toggleSubMenu}>
      {props.children}
    </a>
  )

  const renderRouteLink = () => (
    <RouteLink to={props.path} className={_className()}>
      {props.children}
    </RouteLink>
  )

  return (
    <div onMouseOver={props.handleHover} onMouseOut={props.handleHover}>
      {props.hasSubmenu ? renderSimpleLink() : renderRouteLink()}
    </div>
  )
}

export default enhance(Link)
