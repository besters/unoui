```jsx
<Sidebar>
  <Menu>
    <Item path="/bar" label="Bar" />
    <MenuHeader>Some Header</MenuHeader>
    <Item path="/baz" label="Baz" />
  </Menu>
</Sidebar>
```
