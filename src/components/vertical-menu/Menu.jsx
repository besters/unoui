// @flow

import React from 'react'
import cx from 'classnames'
import { startsWith } from 'lodash'

import Container from './Container.jsx'

import type { ChildrenArray, Node } from 'react'

import './menu.scss'

type Props = {
  /** Items */
  children: ChildrenArray<Node>,

  /** Keep open only one submenu */
  accordion?: boolean,

  /** Navigation color. Currently supports only dark shades */
  color?: string,

  /** Secondary color for active item. Not used when not set. */
  accentColor?: string,

  /** Disable boxed iconss */
  flat?: false,

  /** Additional css classes */
  className?: string,
}

type State = {
  baseColor: string,
}

/**
 * Main menu wrapper
 */
class Menu extends React.Component<Props, State> {
  static defaultProps = {
    color: 'teal-90',
    accordion: false,
    flat: false,
  }

  state = {
    baseColor: '',
  }

  _menu = ''

  componentDidMount = () => {
    const backgroundStyle = window
      .getComputedStyle(this._menu, null)
      .getPropertyValue('background-color')

    this.setState({ baseColor: backgroundStyle })
  }

  _getBgColor = () => {
    const isCustomColor = startsWith(this.props.color, '#')
    if (isCustomColor) return ''

    return this.props.color ? `bg-${this.props.color}` : ''
  }

  _classes = () => cx(this._getBgColor(), 'Menu', this.props.className)

  _style = () => {
    const isCustomColor = startsWith(this.props.color, '#')
    if (!isCustomColor) return {}

    return {
      backgroundColor: this.props.color,
    }
  }

  render = () => (
    <div
      ref={menu => (this._menu = menu)}
      className={this._classes()}
      style={this._style()}>
      <Container
        items={this.props.children}
        accordion={this.props.accordion}
        baseColor={this.state.baseColor}
        accentColor={this.props.accentColor}
        flat={this.props.flat}
        isOpen
        isTopLevel
      />
    </div>
  )
}

export default Menu
