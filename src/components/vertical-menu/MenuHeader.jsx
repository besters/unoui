// @flow

import React from 'react'

import type { Node } from 'react'

type Props = {
  children: Node,
}

const MenuHeader = (props: Props) => (
  <li className="Menu__Header white o-50 fw5 ttu f7 mv3">{props.children}</li>
)

export default MenuHeader
