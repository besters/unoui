// @flow

import React from 'react'
import Icon from 'react-icons-kit'
import { Route } from 'react-router-dom'
import { Motion, spring } from 'react-motion'
import { ic_keyboard_arrow_right } from 'react-icons-kit/md/ic_keyboard_arrow_right'

import Container from './Container.jsx'
import Link from './Link.jsx'
import IconOrLabel from './Icon.jsx'

import type { ChildrenArray, Node } from 'react'

type Props = {
  /** Link content. Can be string, or JSX tag. */
  label: string,

  /** Link target */
  path: string,

  /** Icon component */
  icon?: Node,

  /** When true, will only match if the path matches the location.pathname exactly. */
  exact: boolean,

  /** Disable boxed icons */
  flat: boolean,

  /** Keep open only one submenu */
  accordion: boolean,

  /** Secondary color for active item. */
  accentColor: string,

  /** Submenu links */
  children: ChildrenArray<*>,

  /** Has open submenu
  * @ignore
  */
  submenuIsOpen: boolean,

  /** Menu background
  * @ignore
  */
  baseColor: string,

  /** Not a submenu
  * @ignore
  */
  isTopLevel: boolean,

  /** Function that can toggle submenu
  * @ignore
  */
  toggleSubMenu: (e: SyntheticEvent<*>) => void,
}

const _hasSubmenu = (children: ChildrenArray<*>) =>
  React.Children.count(children) > 0

const _renderSubmenu = (props: Props) =>
  _hasSubmenu(props.children) && (
    <Container
      items={props.children}
      isOpen={props.submenuIsOpen}
      accordion={props.accordion}
      baseColor={props.baseColor}
      accentColor={props.accentColor}
      flat={props.flat}
      isTopLevel={false}
    />
  )

const _renderTriangle = (children: ChildrenArray<Node>, active: boolean) =>
  _hasSubmenu(children) && (
    <Motion style={{ x: spring(active ? 90 : 0, { stiffness: 300 }) }}>
      {({ x }) => (
        <span
          className="Menu__flex-right mr2"
          style={{ transform: `rotate(${x}deg)` }}>
          <Icon size={15} icon={ic_keyboard_arrow_right} />
        </span>
      )}
    </Motion>
  )

const Item = (props: Props) => (
  <li>
    <Route path={props.path} exact={props.exact}>
      {({ match }) => {
        const hasSubmenu = _hasSubmenu(props.children)
        return (
          <Link
            path={props.path}
            exact={props.exact}
            isActive={!!match && !hasSubmenu}
            hasSubmenu={hasSubmenu}
            toggleSubMenu={props.toggleSubMenu}
            submenuIsOpen={props.submenuIsOpen}
            baseColor={props.baseColor}
            isTopLevel={props.isTopLevel}>
            <span className="flex items-center w-100">
              <IconOrLabel
                icon={props.icon}
                label={props.label}
                accentColor={props.accentColor}
                match={!!match && !hasSubmenu}
                isTopLevel={props.isTopLevel}
                flat={props.flat}
              />
              {props.label}
              {_renderTriangle(props.children, props.submenuIsOpen)}
            </span>
          </Link>
        )
      }}
    </Route>

    {_renderSubmenu(props)}
  </li>
)

export default Item
