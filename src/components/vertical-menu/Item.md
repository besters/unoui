```jsx
<Sidebar>
  <Menu>
    <Item path="/default" label="Default item" />
    <Item path="/icon" label="With icon" icon={<FaGithub size={19} />} />
    <Item path="/flat" label="Flat" flat />
    <Item path="/flat_icon" label="Flat with icon" icon={<FaGithub size={19} />} flat />
    <Item path="/accent" label="Accent color (when active)" accentColor="deep-purple" />
    <Item label="Menu 1">
      <Item label="Menu 2" flat>
        <Item label="Menu 3">
          <Item path="/lvl4" label="Menu 4" />
        </Item>
      </Item>
    </Item>
  </Menu>
</Sidebar>
```
