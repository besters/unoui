// @flow

import React from 'react'
import cx from 'classwrap'

import type { Node } from 'react'

type Props = {
  icon?: Node,
  label: string,
  accentColor: string,
  match: boolean,
  isTopLevel: boolean,
  flat: boolean,
}

const unicode_charAt = function(string, index) {
  const first = string.charCodeAt(index)
  let second
  if (first >= 0xd800 && first <= 0xdbff && string.length > index + 1) {
    second = string.charCodeAt(index + 1)
    if (second >= 0xdc00 && second <= 0xdfff) {
      return string.substring(index, index + 2)
    }
  }
  return string[index]
}

const unicode_slice = function(string, start, end) {
  let accumulator = ''
  let character
  let stringIndex = 0
  let unicodeIndex = 0
  const length = string.length

  while (stringIndex < length) {
    character = unicode_charAt(string, stringIndex)
    if (unicodeIndex >= start && unicodeIndex < end) {
      accumulator += character
    }
    stringIndex += character.length
    unicodeIndex += 1
  }
  return accumulator
}

const box = (props: Props) =>
  cx([
    {
      'Menu__dark--20':
        !props.accentColor || (props.accentColor && !props.match),
      [`bg-${props.accentColor}`]: props.accentColor && props.match,
    },
  ])

const classes = (props: Props) =>
  cx([
    'dib',
    'flex',
    'items-center',
    'justify-center',
    'b',
    'mr2',
    'f7',
    {
      [box(props)]: !props.flat,
    },
  ])

const label = (props: Props) => {
  const words = props.label.split(' ')

  if (words.length > 1) {
    return `${unicode_slice(words[0], 0, 1)}${unicode_slice(words[1], 0, 1)}`
  } else {
    return unicode_slice(props.label, 0, 2)
  }
}

const Icon = (props: Props) => (
  <span
    className={classes(props)}
    style={{ width: '1.8rem', height: '1.8rem' }}>
    {props.icon ? props.icon : label(props)}
  </span>
)

export default Icon
