// @flow

import React from 'react'
import { connect } from 'react-redux'

import { toggleHide, toggleMinify } from '../sidebar/actions.jsx'

import type { Node } from 'react'

type Props = {
  /** Sidebar identifier */
  id: string,

  /** Wrapped component */
  children: (props: Object) => Node,

  /** Should collapse and expand sidebar */
  collapse?: boolean,

  /** Should hide and show sidebar */
  hide?: boolean,

  /**
   * Is the sidebar collapsed
   * @ignore
   */
  collapsed: boolean,

  /**
   * Is the sidebar hidden
   * @ignore
   */
  hidden: boolean,

  /**
   * Action to toggle collapse
   * @ignore
   */
  toggleMinify: (id: string) => void,

  /**
   * Action to toggle hide
   * @ignore
   */
  toggleHide: (id: string) => void,
}

const SidebarControl = (props: Props) => {
  const _toggleAction = e => {
    e.preventDefault()

    // When no id is set, do nothing
    if (!props.id) return

    if (props.collapse) props.toggleMinify(props.id)
    if (props.hide) props.toggleHide(props.id)
  }

  return (
    <div>
      {props.children({
        toggle: _toggleAction,
        isCollapsed: props.collapsed,
        isHidden: props.hidden,
      })}
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps

  return {
    collapsed: state.unoui.getIn(['sidebars', id, 'mini']),
    hidden: state.unoui.getIn(['sidebars', id, 'hide']),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toggleHide: param => {
      dispatch(toggleHide(param))
    },
    toggleMinify: param => {
      dispatch(toggleMinify(param))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarControl)
