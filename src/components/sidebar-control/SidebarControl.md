Basic collapse link example
```jsx static
<SidebarControl id="mainNav" collapse>
  {({ toggle, isCollapsed }) => {
    return (
      <a href="#" onClick={toggle}>
        {isCollapsed ? 'Expand' : 'Collapse'}
      </a>
    )
  }}
</SidebarControl>
```

Hide/Show with icon example
```jsx static
<SidebarControl id="mainNav" hide>
  {({ toggle, isHidden }) => {
    return (
      <a href="#" onClick={toggle}>
        {isHidden ? <Icon icon="show" /> : <Icon icon="hide" />}
      </a>
    )
  }}
</SidebarControl>
```

```jsx
<div className="h5 flex">
  <Sidebar id="mainNav" color="slate-90" />

  <div>
    <SidebarControl id="mainNav" hide>
      {({ toggle, isHidden }) => {
        return (
          <a href="#" onClick={toggle}>
            {isHidden ? 'Show' : 'Hide'}
          </a>
        )
      }}
    </SidebarControl>

    <SidebarControl id="mainNav" collapse>
      {({ toggle, isCollapsed }) => {
        return (
          <a href="#" onClick={toggle}>
            {isCollapsed ? 'Expand' : 'Collapse'}
          </a>
        )
      }}
    </SidebarControl>
  </div>
</div>
```