// @flow

import React from 'react'
import { Helmet } from 'react-helmet'

import type { Node } from 'react'

type PropTypes = {
  /** Actual page content */
  children: Node,

  /** Page title */
  title?: string,

  /** Additional components to render at the top of the page. Usually additional navbars */
  topArea?: Node,

  /** Additional components to render on left side of the page. Usually additional sidebars */
  leftArea?: Node,

  /** Additional components to render on right side of the page. Usually additional sidebars */
  rightArea?: Node,
}

const _renderSidebar = (sidebar?: Node) => {
  if (!sidebar) return null

  return React.cloneElement(sidebar, {
    detached: true,
  })
}

/**
* Page Wrapper
*/
const Page = (props: PropTypes) => (
  <div className="ph3">
    {props.title && <Helmet title={props.title} />}

    {props.topArea && props.topArea}

    <div className="flex">
      {_renderSidebar(props.leftArea)}
      <div className="ph3">{props.children}</div>
      {_renderSidebar(props.rightArea)}
    </div>
  </div>
)

export default Page
