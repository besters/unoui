### Real Life example
Page is typically wrapped in a layout. Every single page (controller action) should be wrapped in Page to preserve uniformity

```jsx static
<Layout>
  <Page
    title="Page title"
    rightArea={<Sidebar color="slate-90" />}
    leftArea={<Sidebar color="slate-90" />}
    topArea={<Navbar brandTitle="Hellooo" />}>
    <h4>Page content</h4>
  </Page>
</Layout>
```