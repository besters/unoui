// @flow

import React from 'react'
import { Helmet } from 'react-helmet'

import type { Node } from 'react'

type PropTypes = {
  /** Actual page content */
  children: Node,

  /**
   * Configuration object for react-helmet
   *
   * @param {string} defaultTitle
   * @param {string} titleTemplate
   */
  helmet: Object,

  /** Page title */
  title?: string,

  /** Hide main navigation sidebar */
  hideSidebar?: boolean,

  /** Hide main topbar */
  hideNavbar?: boolean,

  /** Additional components to render under top navbar. Usually additional navbars */
  topArea?: Node,

  /** Additional components to render on left side next to sidebar. Usually additional sidebars */
  leftArea?: Node,

  /** Additional components to render on the right side of page. Usually additional sidebars */
  rightArea?: Node,

  /** Element used as top navigation bar */
  navbar?: Node,

  /** Element used as left sidebar with menu */
  sidebar?: Node,
}

const _title = (helmet: Object, title: ?string) => (
  <Helmet {...helmet} title={title} />
)

const _renderNavbar = (hide: ?boolean, navbar?: Node) => {
  if (!navbar || hide) return null

  return navbar
}

const _renderArea = (component?: Node) => component && component

const _renderSidebar = (hide: ?boolean, sidebar?: Node) => {
  if (!sidebar || hide) return null

  return sidebar
}

const _renderContent = (children: Node) => (
  <div className="grow-1">{children}</div>
)

/**
* This is basic layout, intended to override in the application. Especially the required parameters
*/
const Layout = (props: PropTypes) => (
  <div className="min-vh-100 flex flex-column">
    {_title(props.helmet, props.title)}

    {_renderNavbar(props.hideNavbar, props.navbar)}
    {_renderArea(props.topArea)}

    <div className="flex grow-1">
      {_renderSidebar(props.hideSidebar, props.sidebar)}
      {_renderArea(props.leftArea)}
      {_renderContent(props.children)}
      {_renderArea(props.rightArea)}
    </div>
  </div>
)

Layout.defaultProps = {
  hideSidebar: false,
  hideNavbar: false,
}

export default Layout
