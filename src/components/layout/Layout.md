Jednotlivé stránky v koncové aplikaci by měly být obaleny do layoutu.
UnoUI obsahuje defaultní layout, který je ale potřeba nejdříve řádně nastavit.

```jsx static
const AppLayout = (props) => (
  <Layout
    navbar={<NavbarComponent />}
    sidebar={<SidebarComponent />}
    helmet={{
      defaultTitle: 'Application name',
      titleTemplate: 'MySite.com | %s'
    }}
    {...props}
  />
)
```

A potom v aplikaci používát Aplikační layout, včetně jakýchkoliv props.
```jsx static
const SomePage = (props) => (
  <AppLayout title="Page title" hideSidebar>
    Page content
  </AppLayout>
)
```