import { compose, createStore, combineReducers } from 'redux'

import concatenateReducers from 'redux-concatenate-reducers'

import sidebarReducer from '../components/sidebar/reducer.jsx'

const reducer = concatenateReducers([sidebarReducer])

const reducers = combineReducers({
  unoui: reducer,
})

const devTool = window.devToolsExtension ? window.devToolsExtension() : f => f

export default createStore(reducers, compose(devTool))
