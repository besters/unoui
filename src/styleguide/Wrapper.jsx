import React from 'react'

const { MemoryRouter } = require('react-router-dom')
const { Provider } = require('react-redux')
const configureStore = require('./store.jsx').default

export default class Wrapper extends React.Component {
  render() {
    return (
      <Provider store={configureStore}>
        <MemoryRouter>{this.props.children}</MemoryRouter>
      </Provider>
    )
  }
}
