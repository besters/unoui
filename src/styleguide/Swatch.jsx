import React from 'react'

const Swatch = ({ color }) =>
  <div className="pa3 fl w-third">
    <div className="f5 fw5 br2 shadow-1">
      <div className={`ttc f4 fw5 tal pa3 h4 br2 br--top bg-${color}`}>
        {color}
      </div>
      <div className={`bg-${color}-05 pa3`}>05</div>
      <div className={`bg-${color}-10 pa3`}>10</div>
      <div className={`bg-${color}-20 pa3`}>20</div>
      <div className={`bg-${color}-30 pa3`}>30</div>
      <div className={`bg-${color}-40 pa3`}>40</div>
      <div className={`bg-${color}-50 pa3`}>50</div>
      <div className={`bg-${color}-60 pa3`}>60</div>
      <div className={`bg-${color}-70 pa3`}>70</div>
      <div className={`bg-${color}-80 pa3`}>80</div>
      <div className={`bg-${color}-90 pa3`}>90</div>
      <div className="pa3">
        <small className="white-80 pt2 db">Variable</small>
        <code className="db white-70 f5">{`var(--${color}-*)`}</code>

        <small className="white-80 pt2 db">Background</small>
        <code className="db white-70 f5">{`.bg-${color}-*`}</code>
        <code className="db white-70 f5">{`.hover-bg-${color}-*`}</code>

        <small className="white-80 pt2 db">Text</small>
        <code className="db white-70 f5">{`.${color}-*`}</code>
        <code className="db white-70 f5">{`.hover-${color}-*`}</code>

        <small className="white-80 pt2 db">Borders</small>
        <code className="db white-70 f5">{`.b--${color}-*`}</code>
        <code className="db white-70 f5">{`.b--top-${color}-*`}</code>
        <code className="db white-70 f5">{`.b--right-${color}-*`}</code>
        <code className="db white-70 f5">{`.b--bottom-${color}-*`}</code>
        <code className="db white-70 f5">{`.b--lelft-${color}-*`}</code>
      </div>
    </div>
  </div>

export default Swatch
