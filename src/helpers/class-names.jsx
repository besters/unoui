// @flow

// Returns bg class name from color name
export const _bg = (bg?: string): string => (bg ? `bg-${bg}` : '')

// Returns font size
export const _fs = (size?: number): string => (size ? `f${size}` : '')
