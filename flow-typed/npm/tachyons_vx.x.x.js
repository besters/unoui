// flow-typed signature: 37431e9ee0fad1565967924aaf2731ea
// flow-typed version: <<STUB>>/tachyons_v^4.8.1/flow_v0.54.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'tachyons'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'tachyons' {
  declare module.exports: any;
}
