```shell
yarn add git+ssh://git@gitlab.com:unodor/unoui.git
```

```scss
// Local variables
@import './override-variables.scss';

// Global variables
@import './core/variables/global.scss';

// Color helpers
// Separate file in production
@import './entries/colors.scss';

// Typography and layout helpers
// Separate file in production
@import './entries/helpers.scss';

// Basic scaffolding & typography
// Separate file in production
@import './entries/typography.scss';
```