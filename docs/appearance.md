#### Default link
```jsx
<a href="#">Link name</a>
```

### Default paragraph
```jsx
<p>Text purple 30</p>
```

#### Heading styles
```jsx
<div>
  <h1>H1 Heading</h1>
  <h2>H2 Heading</h2>
  <h3>H3 Heading</h3>
  <h4>H4 Heading</h4>
  <h5>H5 Heading</h5>
  <h6>H6 Heading</h6>
</div>
```
