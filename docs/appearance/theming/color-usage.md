#### Background color
```jsx
<div>
  <div className="bg-teal-30 dib pa3 ma3">.bg-teal-30</div>
  <div className="bg-amber-70 dib pa3 ma3">.bg-amber-70</div>
  <div className="bg-purple dib pa3 ma3">.bg-purple</div>
  <div className="bg-indigo-10 dib pa3 ma3">.bg-indigo-10</div>
  <div className="bg-brown-90 dib pa3 ma3">.bg-brown-90</div>
  <div className="bg-red-05 dib pa3 ma3">.bg-red-05</div>
</div>
```

#### Background color on hover
```jsx
<div>
  <div className="hover-bg-teal-30 bg-animate shadow-1 dib pa3 ma3">.hover-bg-teal-30</div>
  <div className="hover-bg-amber-70 bg-animate shadow-1 dib pa3 ma3">.hover-bg-amber-70</div>
  <div className="hover-bg-indigo bg-animate shadow-1 dib pa3 ma3">.hover-bg-indigo</div>
  <div className="hover-bg-red-05 bg-animate shadow-1 dib pa3 ma3">.hover-bg-red-05</div>
</div>
```

#### Text color
```jsx
<div>
  <p className="brown">.brown</p>
  <p className="amber-60">.amber-60</p>
  <p className="cyan-30">.cyan-30</p>
  <a href="#" className="red underline">.red link</a>
</div>
```

#### Text color on hover
```jsx
<div>
  <p className="hover-brown">.hover-brown</p>
  <p className="hover-amber-60">.hover-amber-60</p>
  <p className="hover-cyan-30">.hover-cyan-30</p>
  <a href="#" className="hover-red underline">.hover-red link</a>
</div>
```

#### Borders color
```jsx
<div>
  <div className="b--red ba pa2 ma2 dib">.b--red</div>
  <div className="b--t-purple-05 b--r-purple-30 b--b-purple-50 b--l-purple-80 bw2 ba pa2 ma2 dib">.b--t-purple-05 .b--r-purple-30 .b--b-purple-50 .b--l-purple-80</div>
</div>
```

#### Combinations
```jsx
<div>
  <a href="#" className="red bg-yellow b--blue hover-green-70 hover-bg-purple-20 ba dib pa2">Modern link button</a>
</div>
```