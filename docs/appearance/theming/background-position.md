```css
/*
    BACKGROUND POSITION

    Base:
    bg = background

    Modifiers:
    -center = center center
    -top = top center
    -right = center right
    -bottom = bottom center
    -left = center left

    Media Query Extensions:
      -ns = not-small
      -m  = medium
      -l  = large
 */

.bg-center {
  background-repeat: no-repeat;
  background-position: center center;
}

.bg-top {
  background-repeat: no-repeat;
  background-position: top center;
}

.bg-right {
  background-repeat: no-repeat;
  background-position: center right;
}

.bg-bottom {
  background-repeat: no-repeat;
  background-position: bottom center;
}

.bg-left {
  background-repeat: no-repeat;
  background-position: center left;
}
```

`.bg-top`
```jsx
<div className="w-100 h5 bg-white-10">
  <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="bg-top h-100 w-100"></div>
</div>
```

`.bg-right`
```jsx
<div className="w-100 h5 bg-white-10">
  <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="bg-right h-100 w-100"></div>
</div>
```

`.bg-bottom`
```jsx
<div className="w-100 h5 bg-white-10">
  <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="bg-bottom h-100 w-100"></div>
</div>
```

`.bg-left`
```jsx
<div className="w-100 h5 bg-white-10">
  <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="bg-left h-100 w-100"></div>
</div>
```

`.bg-center`
```jsx
<div className="w-100 h5 bg-white-10">
  <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="bg-center h-100 w-100"></div>
</div>
```
