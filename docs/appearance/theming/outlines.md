```css
/*
   OUTLINES

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.outline { outline: 1px solid; }
.outline-transparent { outline: 1px solid transparent; }
.outline-0 { outline: 0; }
```

`.outline`
```jsx
<input type="text" className="outline" />
```

`.outline-transparent`
```jsx
<input type="text" className="outline-transparent" />
```

`.outline-0`
```jsx
<input type="text" className="outline-0" />