```css
/*
    BORDERS
    Docs: http://tachyons.io/docs/themes/borders/

    Base:
      b = border

    Modifiers:
      a = all
      t = top
      r = right
      b = bottom
      l = left
      n = none

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

  .ba { border-style: solid; border-width: 1px; }
  .bt { border-top-style: solid; border-top-width: 1px; }
  .br { border-right-style: solid; border-right-width: 1px; }
  .bb { border-bottom-style: solid; border-bottom-width: 1px; }
  .bl { border-left-style: solid; border-left-width: 1px; }
  .bn { border-style: none; border-width: 0; }
```

```css
/*
   BORDER STYLES
   Docs: http://tachyons.io/docs/themes/borders/

   Depends on base border module in _borders.css

   Base:
     b = border-style

   Modifiers:
     --none   = none
     --dotted = dotted
     --dashed = dashed
     --solid  = solid

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
 */

.b--dotted { border-style: dotted; }
.b--dashed { border-style: dashed; }
.b--solid {  border-style: solid; }
.b--none {   border-style: none; }
```

```css
/*
   BORDER WIDTHS
   Docs: http://tachyons.io/docs/themes/borders/

   Base:
     bw = border-width

   Modifiers:
     0 = 0 width border
     1 = 1st step in border-width scale
     2 = 2nd step in border-width scale
     3 = 3rd step in border-width scale
     4 = 4th step in border-width scale
     5 = 5th step in border-width scale

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.bw0 { border-width: 0; }
.bw05 { border-width: 1px; }
.bw1 { border-width: .125rem; }
.bw2 { border-width: .25rem; }
.bw3 { border-width: .5rem; }
.bw4 { border-width: 1rem; }
.bw5 { border-width: 2rem; }

/* Resets */
.bt-0 { border-top-width: 0; }
.br-0 { border-right-width: 0; }
.bb-0 { border-bottom-width: 0; }
.bl-0 { border-left-width: 0; }
```

### BORDER BASE

`.ba`
```jsx
<div className="ba pv3 bg-white-10 mw5"></div>
```

`.bt`
```jsx
<div className="bt pv3 bg-white-10 mw5"></div>
```

`.br`
```jsx
<div className="br pv3 bg-white-10 mw5"></div>
```

`.bb`
```jsx
<div className="bb pv3 bg-white-10 mw5"></div>
```

`.bl`
```jsx
<div className="bl pv3 bg-white-10 mw5"></div>
```

### BORDER STYLES

`.b--dashed`
```jsx
<div className="ba b--dashed pv3 bg-white-10 mw6"></div>
```

`.b--dotted`
```jsx
<div className="ba b--dotted pv3 bg-white-10 mw6"></div>
```

### BORDER WIDTHS

`.bw0`
```jsx
<div className="ba bw0 pv3 bg-white-10 mw6"></div>
```

`.bw1`
```jsx
<div className="ba bw1 pv3 bg-white-10 mw6"></div>
```

`.bw05`
```jsx
<div className="ba bw05 pv3 bg-white-10 mw6"></div>
```

`.bw2`
```jsx
<div className="ba bw2 pv3 bg-white-10 mw6"></div>
```

`.bw3`
```jsx
<div className="ba bw3 pv3 bg-white-10 mw6"></div>
```

`.bw4`
```jsx
<div className="ba bw4 pv3 bg-white-10 mw6"></div>
```

`.bw5`
```jsx
<div className="ba bw5 pv3 bg-white-10 mw6"></div>
```

`.bw2 .br-0 .bl-0`
```jsx
<div className="ba bw2 br-0 bl-0 pv3 bg-white-10 mw6"></div>
```
