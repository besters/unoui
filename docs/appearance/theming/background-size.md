```css
/*
   BACKGROUND SIZE
   Docs: http://tachyons.io/docs/themes/background-size/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

/*
  Often used in combination with background image set as an inline style
  on an html element.
*/
  .cover { background-size: cover!important; }
  .contain { background-size: contain!important; }
```

```jsx
<div className="cf">
  <div className="fl w-50 pr4">
    <code className="fw4 dib">.contain</code>
    <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="contain bg-left h4 w-100"></div>
  </div>
  <div className="fl w-50 pr4">
    <code className="fw4 dib">.cover</code>
    <div style={{backgroundImage: "url(http://placekitten.com/g/500/500)"}} className="cover bg-center h4 w-100"></div>
  </div>
</div>
```