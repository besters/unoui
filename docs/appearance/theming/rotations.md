```css
/*
   ROTATIONS
*/

.rotate-45 { transform: rotate(45deg); }
.rotate-90 { transform: rotate(90deg); }
.rotate-135 { transform: rotate(135deg); }
.rotate-180 { transform: rotate(180deg); }
.rotate-225 { transform: rotate(225deg); }
.rotate-270 { transform: rotate(270deg); }
.rotate-315 { transform: rotate(315deg); }
```

```jsx
<div className="cf bg-white-10 pa5">
  <div className="pa2 bg-black white dib rotate-45">.rotate-45</div>
  <div className="pa2 bg-black white dib rotate-90">.rotate-90</div>
  <div className="pa2 bg-black white dib rotate-135">.rotate-135</div>
  <div className="pa2 bg-black white dib rotate-180">.rotate-180</div>
  <div className="pa2 bg-black white dib rotate-225">.rotate-225</div>
  <div className="pa2 bg-black white dib rotate-270">.rotate-270</div>
  <div className="pa2 bg-black white dib rotate-315">.rotate-315</div>
</div>
```
