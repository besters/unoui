```css
/*
    OPACITY
    Docs: http://tachyons.io/docs/themes/opacity/
*/

.o-100 { opacity: 1;    }
.o-90  { opacity: .9;   }
.o-80  { opacity: .8;   }
.o-70  { opacity: .7;   }
.o-60  { opacity: .6;   }
.o-50  { opacity: .5;   }
.o-40  { opacity: .4;   }
.o-30  { opacity: .3;   }
.o-20  { opacity: .2;   }
.o-10  { opacity: .1;   }
.o-05  { opacity: .05;  }
.o-025 { opacity: .025; }
.o-0   { opacity: 0; }
```

```jsx
<div className="cf">
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-90"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-90" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-80"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-80" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-70"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-70" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-60"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-60" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-50"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-50" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-40"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-40" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-30"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-30" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-20"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-20" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-10"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-10" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-05"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-05" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-025"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-025" />
  </div>
  <div className="fl w-33 pt4">
    <code className="db mv3">&lt;img src="#" class="o-0"&gt;</code>
    <img src="https://unsplash.it/380/250" className="w-100 o-0" />
  </div>
</div>
```