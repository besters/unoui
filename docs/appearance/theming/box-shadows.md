### Box shadows

```jsx
<div className="flex pv4 f6">
  <div className="dib bg-white black pa3 mh4 shadow-0">.shadow-0</div>
  <div className="dib bg-white black pa3 mh4 shadow-1">.shadow-1</div>
  <div className="dib bg-white black pa3 mh4 shadow-2">.shadow-2</div>
  <div className="dib bg-white black pa3 mh4 shadow-3">.shadow-3</div>
  <div className="dib bg-white black pa3 mh4 shadow-4">.shadow-4</div>
  <div className="dib bg-white black pa3 mh4 shadow-5">.shadow-5</div>
</div>
```

### Bos shadows hover animations
```jsx
<div className="flex pv4 f6">
  <div className="dib bg-white black pa3 mh2 shadow-1 shadow-hover-0">.shadow-hover-0</div>
  <div className="dib bg-white black pa3 mh2 shadow-hover-1">.shadow-hover-1</div>
  <div className="dib bg-white black pa3 mh2 shadow-hover-2">.shadow-hover-2</div>
  <div className="dib bg-white black pa3 mh2 shadow-hover-3">.shadow-hover-3</div>
  <div className="dib bg-white black pa3 mh2 shadow-hover-4">.shadow-hover-4</div>
  <div className="dib bg-white black pa3 mh2 shadow-hover-5">.shadow-hover-5</div>
</div>
```
