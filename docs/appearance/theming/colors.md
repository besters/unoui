### Basic colors
```jsx
<div className="ttu">
  <div className="bg-black pv2 ph3 ma2 shadow-1 f5 dib">black</div>
  <div className="bg-white pv2 ph3 ma2 shadow-1 f5 dib">white</div>
  <div className="bg-red pv2 ph3 ma2 shadow-1 f5 dib">red</div>
  <div className="bg-pink pv2 ph3 ma2 shadow-1 f5 dib">pink</div>
  <div className="bg-purple pv2 ph3 ma2 shadow-1 f5 dib">purple</div>
  <div className="bg-deep-purple pv2 ph3 ma2 shadow-1 f5 dib">deep-purple</div>
  <div className="bg-indigo pv2 ph3 ma2 shadow-1 f5 dib">indigo</div>
  <div className="bg-blue pv2 ph3 ma2 shadow-1 f5 dib">blue</div>
  <div className="bg-light-blue pv2 ph3 ma2 shadow-1 f5 dib">light-blue</div>
  <div className="bg-cyan pv2 ph3 ma2 shadow-1 f5 dib">cyan</div>
  <div className="bg-teal pv2 ph3 ma2 shadow-1 f5 dib">teal</div>
  <div className="bg-green pv2 ph3 ma2 shadow-1 f5 dib">green</div>
  <div className="bg-light-green pv2 ph3 ma2 shadow-1 f5 dib">light-green</div>
  <div className="bg-lime pv2 ph3 ma2 shadow-1 f5 dib">lime</div>
  <div className="bg-yellow pv2 ph3 ma2 shadow-1 f5 dib">yellow</div>
  <div className="bg-amber pv2 ph3 ma2 shadow-1 f5 dib">amber</div>
  <div className="bg-orange pv2 ph3 ma2 shadow-1 f5 dib">orange</div>
  <div className="bg-deep-orange pv2 ph3 ma2 shadow-1 f5 dib">deep-orange</div>
  <div className="bg-brown pv2 ph3 ma2 shadow-1 f5 dib">brown</div>
  <div className="bg-grey pv2 ph3 ma2 shadow-1 f5 dib">grey</div>
  <div className="bg-slate pv2 ph3 ma2 shadow-1 f5 dib">slate</div>
</div>
```

`color-50` is a base color and can be used without number. So for example `.black` and `.black-50` are identical.

### Shade palette
```jsx noeditor
<div className="cf">
  {Swatch.default({color: 'black'})}
  {Swatch.default({color: 'white'})}
  {Swatch.default({color: 'red'})}
  {Swatch.default({color: 'pink'})}
  {Swatch.default({color: 'purple'})}
  {Swatch.default({color: 'deep-purple'})}
  {Swatch.default({color: 'indigo'})}
  {Swatch.default({color: 'blue'})}
  {Swatch.default({color: 'light-blue'})}
  {Swatch.default({color: 'cyan'})}
  {Swatch.default({color: 'teal'})}
  {Swatch.default({color: 'green'})}
  {Swatch.default({color: 'light-green'})}
  {Swatch.default({color: 'lime'})}
  {Swatch.default({color: 'yellow'})}
  {Swatch.default({color: 'amber'})}
  {Swatch.default({color: 'orange'})}
  {Swatch.default({color: 'deep-orange'})}
  {Swatch.default({color: 'brown'})}
  {Swatch.default({color: 'grey'})}
  {Swatch.default({color: 'slate'})}
</div>
```
