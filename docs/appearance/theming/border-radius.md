```css
/*
   BORDER RADIUS
   Docs: http://tachyons.io/docs/themes/border-radius/

   Base:
     br   = border-radius

   Modifiers:
     0    = 0/none
     1    = 1st step in scale
     2    = 2nd step in scale
     3    = 3rd step in scale
     4    = 4th step in scale

   Literal values:
     -100 = 100%
     -pill = 9999px

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

  .br0 {        border-radius: 0; }
  .br1 {        border-radius: .125rem; }
  .br2 {        border-radius: .25rem; }
  .br3 {        border-radius: .5rem; }
  .br4 {        border-radius: 1rem; }
  .br-100 {     border-radius: 100%; }
  .br-pill {    border-radius: 9999px; }
  .br--bottom {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
  }
  .br--top {
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
  }
  .br--right {
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
  }
  .br--left {
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
  }
```

```jsx
<div className="cf">
  <div className="mh4 ph2 pv4 ba br0 w3 h3 dib">
    <code>.br0</code>
  </div>
  <div className="mh4 ph2 pv4 ba br1 w3 h3 dib">
    <code>.br1</code>
  </div>
  <div className="mh4 ph2 pv4 ba br2 w3 h3 dib">
    <code>.br2</code>
  </div>
  <div className="mh4 ph2 pv4 ba br3 w3 h3 dib">
    <code>.br3</code>
  </div>
  <div className="mh4 ph2 pv4 ba br4 w3 h3 dib">
    <code>.br4</code>
  </div>
  <div className="mh4 ph2 pv4 ba br-100 dib">
    <code>.br-100</code>
  </div>
  <div className="mh4 ph3 pv2 ba br-pill dib">
    <code>.br-pill</code>
  </div>
</div>
```

```jsx
<div className="cf">
  <div className="pa3 mh3 ba br4 br--bottom w-20 dib tc">
    <code>.br--bottom</code>
  </div>
  <div className="pa3 mh3 ba br4 br--top w-20 dib tc">
    <code>.br--top</code>
  </div>
  <div className="pa3 mh3 ba br4 br--right w-20 dib tc">
    <code>.br--right</code>
  </div>
  <div className="pa3 mh3 ba br4 br--left w-20 dib tc">
    <code>.br--left</code>
  </div>
</div>
```