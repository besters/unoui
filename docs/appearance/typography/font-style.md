```css
/*
   FONT STYLE
   Docs: http://tachyons.io/docs/typography/font-style/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.i         { font-style: italic; }
.fs-normal { font-style: normal; }
```

`.i`
```jsx
<div className="i">Readers want what is important to be clearly laid out; they will not read what is too troublesome.</div>
```

`.fs-normal`
```jsx
<i className="fs-normal">Readers want what is important to be clearly laid out; they will not read what is too troublesome.</i>
```
