```css
/*
   TEXT TRANSFORM
   Docs: http://tachyons.io/docs/typography/text-transform/

   Base:
     tt = text-transform

   Modifiers
     c = capitalize
     l = lowercase
     u = uppercase
     n = none

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.ttc { text-transform: capitalize; }
.ttl { text-transform: lowercase; }
.ttu { text-transform: uppercase; }
.ttn { text-transform: none; }
```

`.ttc`
```jsx
<div className="ttc">Capitalized text</div>
```

`.ttl`
```jsx
<div className="ttl">LOWERCASED TEXT</div>
```

`.ttu`
```jsx
<div className="ttu">Uppercased text</div>
```
