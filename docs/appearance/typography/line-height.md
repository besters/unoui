```css
/*
   LINE HEIGHT / LEADING
   Docs: http://tachyons.io/docs/typography/line-height

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

  .lh-solid { line-height: 1; }
  .lh-title { line-height: 1.25; }
  .lh-copy  { line-height: 1.5; }
```

##### .lh-solid
```jsx
<div className="lh-solid">Originally a strip of soft metal used for vertical spacing between lines of type. Now meaning the vertical distance from the baseline of one line to the baseline of the next. Also called leading.</div>
```

##### .lh-title
```jsx
<div className="lh-title">Originally a strip of soft metal used for vertical spacing between lines of type. Now meaning the vertical distance from the baseline of one line to the baseline of the next. Also called leading.</div>
```

##### .lh-copy
```jsx
<div className="lh-copy">Originally a strip of soft metal used for vertical spacing between lines of type. Now meaning the vertical distance from the baseline of one line to the baseline of the next. Also called leading.</div>
```
