Lowercase text should newer be tracked.<br/>
For some larger text, depending on the typeface, a negative tracking might be desirable.

```css
/*
   LETTER SPACING
   Docs: http://tachyons.io/docs/typography/tracking/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.tracked       { letter-spacing:  .1em; }
.tracked-tight { letter-spacing: -.05em; }
.tracked-mega  { letter-spacing:  .25em; }
```

`.tracked`
```jsx
<div className="tracked">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

`.tracked-tight`
```jsx
<div className="tracked-tight">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

`.tracked-mega`
```jsx
<div className="tracked-mega">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```
