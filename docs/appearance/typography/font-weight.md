```css
/*
   FONT WEIGHT
   Docs: http://tachyons.io/docs/typography/font-weight/

   Base
     fw = font-weight

   Modifiers:
     1 = literal value 100
     2 = literal value 200
     3 = literal value 300
     4 = literal value 400
     5 = literal value 500
     6 = literal value 600
     7 = literal value 700
     8 = literal value 800
     9 = literal value 900

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.normal { font-weight: normal; }
.b      { font-weight: bold; }
.fw1    { font-weight: 100; }
.fw2    { font-weight: 200; }
.fw3    { font-weight: 300; }
.fw4    { font-weight: 400; }
.fw5    { font-weight: 500; }
.fw6    { font-weight: 600; }
.fw7    { font-weight: 700; }
.fw8    { font-weight: 800; }
.fw9    { font-weight: 900; }
```

`.normal`
```jsx
<strong className="normal">Font-weight: normal</strong>
```

`.b`
```jsx
<div className="b">Font-weight: bold</div>
```

`.fw?`
```jsx
<div>
  <div className="fw1">Font-weight: 100</div>
  <div className="fw2">Font-weight: 200</div>
  <div className="fw3">Font-weight: 300</div>
  <div className="fw4">Font-weight: 400</div>
  <div className="fw5">Font-weight: 500</div>
  <div className="fw6">Font-weight: 600</div>
  <div className="fw7">Font-weight: 700</div>
  <div className="fw8">Font-weight: 800</div>
  <div className="fw9">Font-weight: 900</div>
</div>
```