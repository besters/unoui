```css
/*
   TYPE SCALE
   Docs: http://tachyons.io/docs/typography/scale/

   Base:
    f = font-size

   Modifiers
     1 = 1st step in size scale
     2 = 2nd step in size scale
     3 = 3rd step in size scale
     4 = 4th step in size scale
     5 = 5th step in size scale
     6 = 6th step in size scale
     7 = 7th step in size scale

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

/*
 * For Hero/Marketing Titles
 *
 * These generally are too large for mobile
 * so be careful using them on smaller screens.
 * */
.f-6,
.f-headline {
  font-size: 6rem;
}
.f-5,
.f-subheadline {
  font-size: 5rem;
}

/* Type Scale */
.f1 { font-size: 3rem; }
.f2 { font-size: 2.25rem; }
.f3 { font-size: 1.5rem; }
.f4 { font-size: 1.25rem; }
.f5 { font-size: 1rem; }
.f6 { font-size: .875rem; }
.f7 { font-size: .75rem; } /* Small and hard to read for many people so use with extreme caution */
```

##### .f-headline
```jsx
<div className="f-headline">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f-subheadline
```jsx
<div className="f-subheadline">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f1
```jsx
<div className="f1">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f2
```jsx
<div className="f2">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f3
```jsx
<div className="f3">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f4
```jsx
<div className="f4">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f5
```jsx
<div className="f5">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f6
```jsx
<div className="f6">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```

##### .f7
```jsx
<div className="f7">A modular scale, like a musical scale, is a prearranged set of harmonious proportions.</div>
```
