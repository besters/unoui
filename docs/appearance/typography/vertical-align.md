```css
/*
   VERTICAL ALIGN
   Docs: http://tachyons.io/docs/typography/vertical-align/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.v-base     { vertical-align: baseline; }
.v-mid      { vertical-align: middle; }
.v-top      { vertical-align: top; }
.v-btm      { vertical-align: bottom; }
```

`.v-base`
```jsx
<div>
  <p className="dib measure v-base">You can say, "I love you," in Helvetica. And you can say it with Helvetica Extra Light if you want to be really fancy. Or you can say it with the Extra Bold if it's really intensive and passionate, you know, and it might work.</p>
  <p className="dib measure v-base">if you want to be really fancy.</p>
</div>
```

`.v-mid`
```jsx
<div>
  <p className="dib measure v-mid">You can say, "I love you," in Helvetica. And you can say it with Helvetica Extra Light if you want to be really fancy. Or you can say it with the Extra Bold if it's really intensive and passionate, you know, and it might work.</p>
  <p className="dib measure v-mid">if you want to be really fancy.</p>
</div>
```

`.v-top`
```jsx
<div>
  <p className="dib measure v-top">You can say, "I love you," in Helvetica. And you can say it with Helvetica Extra Light if you want to be really fancy. Or you can say it with the Extra Bold if it's really intensive and passionate, you know, and it might work.</p>
  <p className="dib measure v-top">if you want to be really fancy.</p>
</div>
```

`.v-btm`
```jsx
<div>
  <p className="dib measure v-btm">You can say, "I love you," in Helvetica. And you can say it with Helvetica Extra Light if you want to be really fancy. Or you can say it with the Extra Bold if it's really intensive and passionate, you know, and it might work.</p>
  <p className="dib measure v-btm">if you want to be really fancy.</p>
</div>
```
