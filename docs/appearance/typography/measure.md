```css
/*
   TYPOGRAPHY
   http://tachyons.io/docs/typography/measure/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

/* Measure is limited to ~66 characters */
.measure {
  max-width: 30em;
}

/* Measure is limited to ~80 characters */
.measure-wide {
  max-width: 34em;
}

/* Measure is limited to ~45 characters */
.measure-narrow {
  max-width: 20em;
}

/* Book paragraph style - paragraphs are indented with no vertical spacing. */
.indent {
  text-indent: 1em;
  margin-top: 0;
  margin-bottom: 0;
}

.small-caps {
  font-variant: small-caps;
}

/* Combine this class with a width to truncate text (or just leave as is to truncate at width of containing element. */
.truncate {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
```

##### .measure
```jsx
<div className="measure">Anything from 45 to 75 characters is widely regarded as a satisfactory length of line for a single-column page...the 66-character line (counting both letters and spaces) is widely regarded as ideal. For multiple-column work, a better average is 40-50 characters.</div>
```

##### .measure-wide
```jsx
<div className="measure-wide">Anything from 45 to 75 characters is widely regarded as a satisfactory length of line for a single-column page...the 66-character line (counting both letters and spaces) is widely regarded as ideal. For multiple-column work, a better average is 40-50 characters.</div>
```

##### .measure-narrow
```jsx
<div className="measure-narrow">Anything from 45 to 75 characters is widely regarded as a satisfactory length of line for a single-column page...the 66-character line (counting both letters and spaces) is widely regarded as ideal. For multiple-column work, a better average is 40-50 characters.</div>
```

##### .indent
```jsx
<div className="indent">Designers create hierarchy and contrast by playing with the scale of letterforms.</div>
```

##### .small-caps
```jsx
<div className="small-caps">Designers create hierarchy and contrast by playing with the scale of letterforms.</div>
```

##### .truncate
```jsx
<div className="truncate">Anything from 45 to 75 characters is widely regarded as a satisfactory length of line for a single-column page...the 66-character line (counting both letters and spaces) is widely regarded as ideal. For multiple-column work, a better average is 40-50 characters.</div>
```
