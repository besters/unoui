```css
/*
   TEXT DECORATION
   Docs: http://tachyons.io/docs/typography/text-decoration/


   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.strike       { text-decoration: line-through; }
.underline    { text-decoration: underline; }
.no-underline { text-decoration: none; }
```

`.strike`
```jsx
<div className="strike">This is striked</div>
```

`.underline`
```jsx
<div className="underline">This is underlined</div>
```
