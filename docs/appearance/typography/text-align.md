```css
/*
  TEXT ALIGN
  Docs: http://tachyons.io/docs/typography/text-align/

  Base
    t = text-align

  Modifiers
    l = left
    r = right
    c = center

  Media Query Extensions:
    -ns = not-small
    -m  = medium
    -l  = large
*/

.tl  { text-align: left; }
.tr  { text-align: right; }
.tc  { text-align: center; }
```

`.tl`
```jsx
<div className="tl">Aligned left</div>
```

`.tr`
```jsx
<div className="tr">Aligned right</div>
```

`.tc`
```jsx
<div className="tc">Aligned center</div>
```
