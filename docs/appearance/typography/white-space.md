```css
/*
   WHITE SPACE
   Docs: http://tachyons.io/docs/typography/white-space/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.ws-normal { white-space: normal; }
.nowrap { white-space: nowrap; }
.pre { white-space: pre; }
```

`.ws-normal`
```jsx
<div className="ws-normal measure">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
```

`.nowrap`
```jsx
<div className="nowrap measure truncate">Typography is to literature as musical performance is to composition: an essential act of interpretation, full of endless opportunities for insight or obtuseness.</div>
```

`.pre`
```jsx
<div className="pre measure">          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</div>
```
