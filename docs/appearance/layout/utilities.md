```css
/*
   UTILITIES

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

/* Equivalent to .overflow-y-scroll */
.overflow-container {
  overflow-y: scroll;
}

.center {
  margin-right: auto;
  margin-left: auto;
}
```

`.center`
```jsx
<div className="w-100 bg-white-10 pv1">
  <div className="bg-white-20 text-black center w5 h3 mv3">.center</div>
</div>
```

`.overflow-container`
```jsx
<div className="w-100 bg-white-10 pv1 overflow-container measure h4">
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce wisi. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam egestas wisi a erat. Duis viverra diam non justo. Aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Donec vitae arcu. Integer pellentesque quam vel velit. In dapibus augue non sapien. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Nunc dapibus tortor vel mi dapibus sollicitudin. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Etiam posuere lacus quis dolor. Nullam dapibus fermentum ipsum. Aliquam erat volutpat. Nunc dapibus tortor vel mi dapibus sollicitudin. Donec vitae arcu.</p>
</div>
```
