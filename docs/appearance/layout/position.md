```css
/*
   POSITIONING
   Docs: http://tachyons.io/docs/layout/position/

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.static { position: static; }
.relative  { position: relative; }
.absolute  { position: absolute; }
.fixed  { position: fixed; }
```

```css
/*
   COORDINATES

   Use in combination with the position module.
*/

.top-0    { top:    0; }
.right-0  { right:  0; }
.bottom-0 { bottom: 0; }
.left-0   { left:   0; }

.top-1    { top:    1rem; }
.right-1  { right:  1rem; }
.bottom-1 { bottom: 1rem; }
.left-1   { left:   1rem; }

.top-2    { top:    2rem; }
.right-2  { right:  2rem; }
.bottom-2 { bottom: 2rem; }
.left-2   { left:   2rem; }

.top--1    { top:    -1rem; }
.right--1  { right:  -1rem; }
.bottom--1 { bottom: -1rem; }
.left--1   { left:   -1rem; }

.top--2    { top:    -2rem; }
.right--2  { right:  -2rem; }
.bottom--2 { bottom: -2rem; }
.left--2   { left:   -2rem; }


.absolute--fill {
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
```

```jsx
<div className="relative bg-white-10 h5">
  <div className="absolute top-0 right-0 bg-black-10 w2 tc white">X</div>
  <p className="measure">This illustrates an absolutely positioned element that might always need to be in the top right of the content like a close button for a modal that needs to be dismissed.</p>
</div>
```

```jsx
<div className="relative bg-white-10 h5">
  <div className="absolute top-1 left-1 right-2 bottom-2 bg-black-50 white">
    <p className="measure">This is an absolutely positioned element set to have offsets of top and left to 1rem with right and bottom offset by 2rem. It's parent with the light gray background is set to position relative.</p>
  </div>
</div>
```

```jsx
<div className="relative h5">
  <div className="absolute absolute--fill bg-black-50 white">
    <p className="measure">This just fills the whole parent</p>
  </div>
</div>
```