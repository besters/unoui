```css
/*
  WORD BREAK

   Base:
     word = word-break

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.word-normal { word-break: normal; }
.word-wrap { word-break: break-all; }
.word-nowrap { word-break: keep-all; }
```

`.word-normal`
```jsx
<div className="word-normal w1">Marco</div>
```

`.word-wrap`
```jsx
<div className="word-wrap w1">Marco</div>
```

`.word-nowrap`
```jsx
<div className="word-nowrap w1">Marco</div>
```
