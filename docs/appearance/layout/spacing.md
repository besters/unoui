```css
/* Variables */

:root {
  --spacing-none: 0;
  --spacing-extra-small: .25rem;
  --spacing-small: .5rem;
  --spacing-medium: 1rem;
  --spacing-large: 2rem;
  --spacing-extra-large: 4rem;
  --spacing-extra-extra-large: 8rem;
  --spacing-extra-extra-extra-large: 16rem;
}

/*
   SPACING
   Docs: http://tachyons.io/docs/layout/spacing/

   An eight step powers of two scale ranging from 0 to 16rem.

   Base:
     p = padding
     m = margin

   Modifiers:
     a = all
     h = horizontal
     v = vertical
     t = top
     r = right
     b = bottom
     l = left

     0 = none
     1 = 1st step in spacing scale
     2 = 2nd step in spacing scale
     3 = 3rd step in spacing scale
     4 = 4th step in spacing scale
     5 = 5th step in spacing scale
     6 = 6th step in spacing scale
     7 = 7th step in spacing scale

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/


.pa0 { padding: var(--spacing-none); }
.pa1 { padding: var(--spacing-extra-small); }
.pa2 { padding: var(--spacing-small); }
.pa3 { padding: var(--spacing-medium); }
.pa4 { padding: var(--spacing-large); }
.pa5 { padding: var(--spacing-extra-large); }
.pa6 { padding: var(--spacing-extra-extra-large); }
.pa7 { padding: var(--spacing-extra-extra-extra-large); }

.pl0 { padding-left: var(--spacing-none); }
.pl1 { padding-left: var(--spacing-extra-small); }
.pl2 { padding-left: var(--spacing-small); }
.pl3 { padding-left: var(--spacing-medium); }
.pl4 { padding-left: var(--spacing-large); }
.pl5 { padding-left: var(--spacing-extra-large); }
.pl6 { padding-left: var(--spacing-extra-extra-large); }
.pl7 { padding-left: var(--spacing-extra-extra-extra-large); }

.pr0 { padding-right: var(--spacing-none); }
.pr1 { padding-right: var(--spacing-extra-small); }
.pr2 { padding-right: var(--spacing-small); }
.pr3 { padding-right: var(--spacing-medium); }
.pr4 { padding-right: var(--spacing-large); }
.pr5 { padding-right: var(--spacing-extra-large); }
.pr6 { padding-right: var(--spacing-extra-extra-large); }
.pr7 { padding-right: var(--spacing-extra-extra-extra-large); }

.pb0 { padding-bottom: var(--spacing-none); }
.pb1 { padding-bottom: var(--spacing-extra-small); }
.pb2 { padding-bottom: var(--spacing-small); }
.pb3 { padding-bottom: var(--spacing-medium); }
.pb4 { padding-bottom: var(--spacing-large); }
.pb5 { padding-bottom: var(--spacing-extra-large); }
.pb6 { padding-bottom: var(--spacing-extra-extra-large); }
.pb7 { padding-bottom: var(--spacing-extra-extra-extra-large); }

.pt0 { padding-top: var(--spacing-none); }
.pt1 { padding-top: var(--spacing-extra-small); }
.pt2 { padding-top: var(--spacing-small); }
.pt3 { padding-top: var(--spacing-medium); }
.pt4 { padding-top: var(--spacing-large); }
.pt5 { padding-top: var(--spacing-extra-large); }
.pt6 { padding-top: var(--spacing-extra-extra-large); }
.pt7 { padding-top: var(--spacing-extra-extra-extra-large); }

.pv0 {
  padding-top: var(--spacing-none);
  padding-bottom: var(--spacing-none);
}
.pv1 {
  padding-top: var(--spacing-extra-small);
  padding-bottom: var(--spacing-extra-small);
}
.pv2 {
  padding-top: var(--spacing-small);
  padding-bottom: var(--spacing-small);
}
.pv3 {
  padding-top: var(--spacing-medium);
  padding-bottom: var(--spacing-medium);
}
.pv4 {
  padding-top: var(--spacing-large);
  padding-bottom: var(--spacing-large);
}
.pv5 {
  padding-top: var(--spacing-extra-large);
  padding-bottom: var(--spacing-extra-large);
}
.pv6 {
  padding-top: var(--spacing-extra-extra-large);
  padding-bottom: var(--spacing-extra-extra-large);
}

.pv7 {
  padding-top: var(--spacing-extra-extra-extra-large);
  padding-bottom: var(--spacing-extra-extra-extra-large);
}

.ph0 {
  padding-left: var(--spacing-none);
  padding-right: var(--spacing-none);
}

.ph1 {
  padding-left: var(--spacing-extra-small);
  padding-right: var(--spacing-extra-small);
}

.ph2 {
  padding-left: var(--spacing-small);
  padding-right: var(--spacing-small);
}

.ph3 {
  padding-left: var(--spacing-medium);
  padding-right: var(--spacing-medium);
}

.ph4 {
  padding-left: var(--spacing-large);
  padding-right: var(--spacing-large);
}

.ph5 {
  padding-left: var(--spacing-extra-large);
  padding-right: var(--spacing-extra-large);
}

.ph6 {
  padding-left: var(--spacing-extra-extra-large);
  padding-right: var(--spacing-extra-extra-large);
}

.ph7 {
  padding-left: var(--spacing-extra-extra-extra-large);
  padding-right: var(--spacing-extra-extra-extra-large);
}

.ma0  {  margin: var(--spacing-none); }
.ma1 {  margin: var(--spacing-extra-small); }
.ma2  {  margin: var(--spacing-small); }
.ma3  {  margin: var(--spacing-medium); }
.ma4  {  margin: var(--spacing-large); }
.ma5  {  margin: var(--spacing-extra-large); }
.ma6 {  margin: var(--spacing-extra-extra-large); }
.ma7 { margin: var(--spacing-extra-extra-extra-large); }

.ml0  {  margin-left: var(--spacing-none); }
.ml1 {  margin-left: var(--spacing-extra-small); }
.ml2  {  margin-left: var(--spacing-small); }
.ml3  {  margin-left: var(--spacing-medium); }
.ml4  {  margin-left: var(--spacing-large); }
.ml5  {  margin-left: var(--spacing-extra-large); }
.ml6 {  margin-left: var(--spacing-extra-extra-large); }
.ml7 { margin-left: var(--spacing-extra-extra-extra-large); }

.mr0  {  margin-right: var(--spacing-none); }
.mr1 {  margin-right: var(--spacing-extra-small); }
.mr2  {  margin-right: var(--spacing-small); }
.mr3  {  margin-right: var(--spacing-medium); }
.mr4  {  margin-right: var(--spacing-large); }
.mr5  {  margin-right: var(--spacing-extra-large); }
.mr6 {  margin-right: var(--spacing-extra-extra-large); }
.mr7 { margin-right: var(--spacing-extra-extra-extra-large); }

.mb0  {  margin-bottom: var(--spacing-none); }
.mb1 {  margin-bottom: var(--spacing-extra-small); }
.mb2  {  margin-bottom: var(--spacing-small); }
.mb3  {  margin-bottom: var(--spacing-medium); }
.mb4  {  margin-bottom: var(--spacing-large); }
.mb5  {  margin-bottom: var(--spacing-extra-large); }
.mb6 {  margin-bottom: var(--spacing-extra-extra-large); }
.mb7 { margin-bottom: var(--spacing-extra-extra-extra-large); }

.mt0  {  margin-top: var(--spacing-none); }
.mt1 {  margin-top: var(--spacing-extra-small); }
.mt2  {  margin-top: var(--spacing-small); }
.mt3  {  margin-top: var(--spacing-medium); }
.mt4  {  margin-top: var(--spacing-large); }
.mt5  {  margin-top: var(--spacing-extra-large); }
.mt6 {  margin-top: var(--spacing-extra-extra-large); }
.mt7 { margin-top: var(--spacing-extra-extra-extra-large); }

.mv0   {
  margin-top: var(--spacing-none);
  margin-bottom: var(--spacing-none);
}
.mv1  {
  margin-top: var(--spacing-extra-small);
  margin-bottom: var(--spacing-extra-small);
}
.mv2   {
  margin-top: var(--spacing-small);
  margin-bottom: var(--spacing-small);
}
.mv3   {
  margin-top: var(--spacing-medium);
  margin-bottom: var(--spacing-medium);
}
.mv4   {
  margin-top: var(--spacing-large);
  margin-bottom: var(--spacing-large);
}
.mv5   {
  margin-top: var(--spacing-extra-large);
  margin-bottom: var(--spacing-extra-large);
}
.mv6  {
  margin-top: var(--spacing-extra-extra-large);
  margin-bottom: var(--spacing-extra-extra-large);
}
.mv7  {
  margin-top: var(--spacing-extra-extra-extra-large);
  margin-bottom: var(--spacing-extra-extra-extra-large);
}

.mh0   {
  margin-left: var(--spacing-none);
  margin-right: var(--spacing-none);
}
.mh1   {
  margin-left: var(--spacing-extra-small);
  margin-right: var(--spacing-extra-small);
}
.mh2   {
  margin-left: var(--spacing-small);
  margin-right: var(--spacing-small);
}
.mh3   {
  margin-left: var(--spacing-medium);
  margin-right: var(--spacing-medium);
}
.mh4   {
  margin-left: var(--spacing-large);
  margin-right: var(--spacing-large);
}
.mh5   {
  margin-left: var(--spacing-extra-large);
  margin-right: var(--spacing-extra-large);
}
.mh6  {
  margin-left: var(--spacing-extra-extra-large);
  margin-right: var(--spacing-extra-extra-large);
}
.mh7  {
  margin-left: var(--spacing-extra-extra-extra-large);
  margin-right: var(--spacing-extra-extra-extra-large);
}
```

### Paddings

`.pa?`
```jsx
<div>
  <div><div className="pa0 dib bg-black white mb2">.pa0</div></div>
  <div><div className="pa1 dib bg-black white mb2">.pa1</div></div>
  <div><div className="pa2 dib bg-black white mb2">.pa2</div></div>
  <div><div className="pa3 dib bg-black white mb2">.pa3</div></div>
  <div><div className="pa4 dib bg-black white mb2">.pa4</div></div>
  <div><div className="pa5 dib bg-black white mb2">.pa5</div></div>
  <div><div className="pa6 dib bg-black white mb2">.pa6</div></div>
  <div><div className="pa7 dib bg-black white mb2">.pa7</div></div>
</div>
```

`.pt?`
```jsx
<div>
  <div><div className="pt0 dib bg-black white mb2">.pt0</div></div>
  <div><div className="pt1 dib bg-black white mb2">.pt1</div></div>
  <div><div className="pt2 dib bg-black white mb2">.pt2</div></div>
  <div><div className="pt3 dib bg-black white mb2">.pt3</div></div>
  <div><div className="pt4 dib bg-black white mb2">.pt4</div></div>
  <div><div className="pt5 dib bg-black white mb2">.pt5</div></div>
  <div><div className="pt6 dib bg-black white mb2">.pt6</div></div>
  <div><div className="pt7 dib bg-black white mb2">.pt7</div></div>
</div>
```

`.pr?`
```jsx
<div>
  <div><div className="pr0 dib bg-black white mb2">.pr0</div></div>
  <div><div className="pr1 dib bg-black white mb2">.pr1</div></div>
  <div><div className="pr2 dib bg-black white mb2">.pr2</div></div>
  <div><div className="pr3 dib bg-black white mb2">.pr3</div></div>
  <div><div className="pr4 dib bg-black white mb2">.pr4</div></div>
  <div><div className="pr5 dib bg-black white mb2">.pr5</div></div>
  <div><div className="pr6 dib bg-black white mb2">.pr6</div></div>
  <div><div className="pr7 dib bg-black white mb2">.pr7</div></div>
</div>
```

`.pb?`
```jsx
<div>
  <div><div className="pb0 dib bg-black white mb2">.pb0</div></div>
  <div><div className="pb1 dib bg-black white mb2">.pb1</div></div>
  <div><div className="pb2 dib bg-black white mb2">.pb2</div></div>
  <div><div className="pb3 dib bg-black white mb2">.pb3</div></div>
  <div><div className="pb4 dib bg-black white mb2">.pb4</div></div>
  <div><div className="pb5 dib bg-black white mb2">.pb5</div></div>
  <div><div className="pb6 dib bg-black white mb2">.pb6</div></div>
  <div><div className="pb7 dib bg-black white mb2">.pb7</div></div>
</div>
```

`.pl?`
```jsx
<div>
  <div><div className="pl0 dib bg-black white mb2">.pl0</div></div>
  <div><div className="pl1 dib bg-black white mb2">.pl1</div></div>
  <div><div className="pl2 dib bg-black white mb2">.pl2</div></div>
  <div><div className="pl3 dib bg-black white mb2">.pl3</div></div>
  <div><div className="pl4 dib bg-black white mb2">.pl4</div></div>
  <div><div className="pl5 dib bg-black white mb2">.pl5</div></div>
  <div><div className="pl6 dib bg-black white mb2">.pl6</div></div>
  <div><div className="pl7 dib bg-black white mb2">.pl7</div></div>
</div>
```

`.pv?`
```jsx
<div>
  <div><div className="pv0 dib bg-black white mb2">.pv0</div></div>
  <div><div className="pv1 dib bg-black white mb2">.pv1</div></div>
  <div><div className="pv2 dib bg-black white mb2">.pv2</div></div>
  <div><div className="pv3 dib bg-black white mb2">.pv3</div></div>
  <div><div className="pv4 dib bg-black white mb2">.pv4</div></div>
  <div><div className="pv5 dib bg-black white mb2">.pv5</div></div>
  <div><div className="pv6 dib bg-black white mb2">.pv6</div></div>
  <div><div className="pv7 dib bg-black white mb2">.pv7</div></div>
</div>
```

`.ph?`
```jsx
<div>
  <div><div className="ph0 dib bg-black white mb2">.ph0</div></div>
  <div><div className="ph1 dib bg-black white mb2">.ph1</div></div>
  <div><div className="ph2 dib bg-black white mb2">.ph2</div></div>
  <div><div className="ph3 dib bg-black white mb2">.ph3</div></div>
  <div><div className="ph4 dib bg-black white mb2">.ph4</div></div>
  <div><div className="ph5 dib bg-black white mb2">.ph5</div></div>
  <div><div className="ph6 dib bg-black white mb2">.ph6</div></div>
  <div><div className="ph7 dib bg-black white mb2">.ph7</div></div>
</div>
```

### Margins

`.ma?`
```jsx
<div>
  <div className="bg-white-20"><div className="ma0 dib bg-black white">.ma0</div></div>
  <div className="bg-white-30"><div className="ma1 dib bg-black white">.ma1</div></div>
  <div className="bg-white-40"><div className="ma2 dib bg-black white">.ma2</div></div>
  <div className="bg-white-50"><div className="ma3 dib bg-black white">.ma3</div></div>
  <div className="bg-white-60"><div className="ma4 dib bg-black white">.ma4</div></div>
  <div className="bg-white-70"><div className="ma5 dib bg-black white">.ma5</div></div>
  <div className="bg-white-80"><div className="ma6 dib bg-black white">.ma6</div></div>
  <div className="bg-white-90"><div className="ma7 dib bg-black white">.ma7</div></div>
</div>
```

`.mt?`
```jsx
<div>
  <div className="bg-white-20"><div className="mt0 dib bg-black white">.mt0</div></div>
  <div className="bg-white-30"><div className="mt1 dib bg-black white">.mt1</div></div>
  <div className="bg-white-40"><div className="mt2 dib bg-black white">.mt2</div></div>
  <div className="bg-white-50"><div className="mt3 dib bg-black white">.mt3</div></div>
  <div className="bg-white-60"><div className="mt4 dib bg-black white">.mt4</div></div>
  <div className="bg-white-70"><div className="mt5 dib bg-black white">.mt5</div></div>
  <div className="bg-white-80"><div className="mt6 dib bg-black white">.mt6</div></div>
  <div className="bg-white-90"><div className="mt7 dib bg-black white">.mt7</div></div>
</div>
```

`.mr?`
```jsx
<div>
  <div className="bg-white-20">
    <div className="mr0 dib bg-black white">.mr0</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-30">
    <div className="mr1 dib bg-black white">.mr1</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-40">
    <div className="mr2 dib bg-black white">.mr2</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-50">
    <div className="mr3 dib bg-black white">.mr3</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-60">
    <div className="mr4 dib bg-black white">.mr4</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-70">
    <div className="mr5 dib bg-black white">.mr5</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-80">
    <div className="mr6 dib bg-black white">.mr6</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-90">
    <div className="mr7 dib bg-black white">.mr7</div>
    <div className="dib pa2 bg-black"></div>
  </div>
</div>
```

`.mb?`
```jsx
<div>
  <div className="bg-white-20"><div className="mb0 dib bg-black white">.mb0</div></div>
  <div className="bg-white-30"><div className="mb1 dib bg-black white">.mb1</div></div>
  <div className="bg-white-40"><div className="mb2 dib bg-black white">.mb2</div></div>
  <div className="bg-white-50"><div className="mb3 dib bg-black white">.mb3</div></div>
  <div className="bg-white-60"><div className="mb4 dib bg-black white">.mb4</div></div>
  <div className="bg-white-70"><div className="mb5 dib bg-black white">.mb5</div></div>
  <div className="bg-white-80"><div className="mb6 dib bg-black white">.mb6</div></div>
  <div className="bg-white-90"><div className="mb7 dib bg-black white">.mb7</div></div>
</div>
```

`.ml?`
```jsx
<div>
  <div className="bg-white-20"><div className="ml0 dib bg-black white">.ml0</div></div>
  <div className="bg-white-30"><div className="ml1 dib bg-black white">.ml1</div></div>
  <div className="bg-white-40"><div className="ml2 dib bg-black white">.ml2</div></div>
  <div className="bg-white-50"><div className="ml3 dib bg-black white">.ml3</div></div>
  <div className="bg-white-60"><div className="ml4 dib bg-black white">.ml4</div></div>
  <div className="bg-white-70"><div className="ml5 dib bg-black white">.ml5</div></div>
  <div className="bg-white-80"><div className="ml6 dib bg-black white">.ml6</div></div>
  <div className="bg-white-90"><div className="ml7 dib bg-black white">.ml7</div></div>
</div>
```

`.mv?`
```jsx
<div>
  <div className="bg-white-20"><div className="mv0 dib bg-black white">.mv0</div></div>
  <div className="bg-white-30"><div className="mv1 dib bg-black white">.mv1</div></div>
  <div className="bg-white-40"><div className="mv2 dib bg-black white">.mv2</div></div>
  <div className="bg-white-50"><div className="mv3 dib bg-black white">.mv3</div></div>
  <div className="bg-white-60"><div className="mv4 dib bg-black white">.mv4</div></div>
  <div className="bg-white-70"><div className="mv5 dib bg-black white">.mv5</div></div>
  <div className="bg-white-80"><div className="mv6 dib bg-black white">.mv6</div></div>
  <div className="bg-white-90"><div className="mv7 dib bg-black white">.mv7</div></div>
</div>
```

`.mh?`
```jsx
<div>
  <div className="bg-white-20">
    <div className="mh0 dib bg-black white">.mh0</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-30">
    <div className="mh1 dib bg-black white">.mh1</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-40">
    <div className="mh2 dib bg-black white">.mh2</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-50">
    <div className="mh3 dib bg-black white">.mh3</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-60">
    <div className="mh4 dib bg-black white">.mh4</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-70">
    <div className="mh5 dib bg-black white">.mh5</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-80">
    <div className="mh6 dib bg-black white">.mh6</div>
    <div className="dib pa2 bg-black"></div>
  </div>
  <div className="bg-white-90">
    <div className="mh7 dib bg-black white">.mh7</div>
    <div className="dib pa2 bg-black"></div>
  </div>
</div>
```
