```css
/*
   CLEARFIX
   http://tachyons.io/docs/layout/clearfix/
*/

/* Nicolas Gallaghers Clearfix solution
   Ref: http://nicolasgallagher.com/micro-clearfix-hack/ */

.cf:before,
.cf:after { content: " "; display: table; }
.cf:after { clear: both; }
.cf {       *zoom: 1; }

.cl { clear: left; }
.cr { clear: right; }
.cb { clear: both; }
.cn { clear: none; }
```

`.cf`
```jsx
<div className="fl">Uncleared float</div>
```

```jsx
<div className="cf">
  <div className="fl">Cleared float</div>
</div>
```