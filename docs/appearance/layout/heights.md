```css
/*
   HEIGHTS
   Docs: http://tachyons.io/docs/layout/heights/

   Base:
     h = height
     min-h = min-height
     min-vh = min-height vertical screen height
     vh = vertical screen height

   Modifiers
     1 = 1st step in height scale
     2 = 2nd step in height scale
     3 = 3rd step in height scale
     4 = 4th step in height scale
     5 = 5th step in height scale

     -25   = literal value 25%
     -50   = literal value 50%
     -75   = literal value 75%
     -100  = literal value 100%

     -auto = string value of auto
     -inherit = string value of inherit

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

/* Height Scale */
.h1 { height: 1rem; }
.h2 { height: 2rem; }
.h3 { height: 4rem; }
.h4 { height: 8rem; }
.h5 { height: 16rem; }

/* Height Percentages - Based off of height of parent */
.h-25 {  height:  25%; }
.h-50 {  height:  50%; }
.h-75 {  height:  75%; }
.h-100 { height: 100%; }

.min-h-100 { min-height: 100%; }

/* Screen Height Percentage */
.vh-25 {  height:  25vh; }
.vh-50 {  height:  50vh; }
.vh-75 {  height:  75vh; }
.vh-100 { height: 100vh; }

.min-vh-100 { min-height: 100vh; }

/* String Properties */
.h-auto {     height: auto; }
.h-inherit {  height: inherit; }
```

`.h?`
```jsx
<div>
  <div className="h1 bg-white-30 mv3">.h1</div>
  <div className="h2 bg-white-30 mv3">.h2</div>
  <div className="h3 bg-white-30 mv3">.h3</div>
  <div className="h4 bg-white-30 mv3">.h4</div>
  <div className="h5 bg-white-30 mv3">.h5</div>
</div>
```

`.h-?`
```jsx
<div className="bg-white-10">
  <div className="h5">
    <div className="h-25 bg-white-30 mb2">.h-25</div>
  </div>
  <div className="h5">
    <div className="h-50 bg-white-30 mb2">.h-50</div>
  </div>
  <div className="h5">
    <div className="h-75 bg-white-30 mb2">.h-75</div>
  </div>
  <div className="h5">
    <div className="h-100 bg-white-30 mb2">.h-100</div>
  </div>
</div>
```


`.vh-?`
```jsx
<div>
  <div className="vh-25 bg-white-30 mv3">.vh-25</div>
  <div className="vh-50 bg-white-30 mv3">.vh-50</div>
  <div className="vh-75 bg-white-30 mv3">.vh-75</div>
  <div className="vh-100 bg-white-30 mv3">.vh-100</div>
</div>
```