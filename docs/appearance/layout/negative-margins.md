```css
/* Variables */

:root {
  --spacing-none: 0;
  --spacing-extra-small: .25rem;
  --spacing-small: .5rem;
  --spacing-medium: 1rem;
  --spacing-large: 2rem;
  --spacing-extra-large: 4rem;
  --spacing-extra-extra-large: 8rem;
  --spacing-extra-extra-extra-large: 16rem;
}

/*
   NEGATIVE MARGINS

   Base:
     n = negative

   Modifiers:
     a = all
     t = top
     r = right
     b = bottom
     l = left

     1 = 1st step in spacing scale
     2 = 2nd step in spacing scale
     3 = 3rd step in spacing scale
     4 = 4th step in spacing scale
     5 = 5th step in spacing scale
     6 = 6th step in spacing scale
     7 = 7th step in spacing scale

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

.na1 { margin: -var(--spacing-extra-small); }
.na2 { margin: -var(--spacing-small); }
.na3 { margin: -var(--spacing-medium); }
.na4 { margin: -var(--spacing-large); }
.na5 { margin: -var(--spacing-extra-large); }
.na6 { margin: -var(--spacing-extra-extra-large); }
.na7 { margin: -var(--spacing-extra-extra-extra-large); }

.nl1 { margin-left: -var(--spacing-extra-small); }
.nl2 { margin-left: -var(--spacing-small); }
.nl3 { margin-left: -var(--spacing-medium); }
.nl4 { margin-left: -var(--spacing-large); }
.nl5 { margin-left: -var(--spacing-extra-large); }
.nl6 { margin-left: -var(--spacing-extra-extra-large); }
.nl7 { margin-left: -var(--spacing-extra-extra-extra-large); }

.nr1 { margin-right: -var(--spacing-extra-small); }
.nr2 { margin-right: -var(--spacing-small); }
.nr3 { margin-right: -var(--spacing-medium); }
.nr4 { margin-right: -var(--spacing-large); }
.nr5 { margin-right: -var(--spacing-extra-large); }
.nr6 { margin-right: -var(--spacing-extra-extra-large); }
.nr7 { margin-right: -var(--spacing-extra-extra-extra-large); }

.nb1 { margin-bottom: -var(--spacing-extra-small); }
.nb2 { margin-bottom: -var(--spacing-small); }
.nb3 { margin-bottom: -var(--spacing-medium); }
.nb4 { margin-bottom: -var(--spacing-large); }
.nb5 { margin-bottom: -var(--spacing-extra-large); }
.nb6 { margin-bottom: -var(--spacing-extra-extra-large); }
.nb7 { margin-bottom: -var(--spacing-extra-extra-extra-large); }

.nt1 { margin-top: -var(--spacing-extra-small); }
.nt2 { margin-top: -var(--spacing-small); }
.nt3 { margin-top: -var(--spacing-medium); }
.nt4 { margin-top: -var(--spacing-large); }
.nt5 { margin-top: -var(--spacing-extra-large); }
.nt6 { margin-top: -var(--spacing-extra-extra-large); }
.nt7 { margin-top: -var(--spacing-extra-extra-extra-large); }
```

`.nl?`
```jsx
<div className="w-100 bg-white-10 pv1">
  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl1">.nl1</div>
  </div>

  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl2">.nl2</div>
  </div>

  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl3">.nl3</div>
  </div>

  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl4">.nl4</div>
  </div>

  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl5">.nl5</div>
  </div>

  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl6">.nl6</div>
  </div>

  <div className="bg-white-20 center w5 h3 mv3">
    <div className="bg-white-40 dib pa1 nl7">.nl7</div>
  </div>
</div>
```
