```css
/*
   DISPLAY
   Docs: http://tachyons.io/docs/layout/display

   Base:
    d = display

   Modifiers:
    n     = none
    b     = block
    ib    = inline-block
    it    = inline-table
    t     = table
    tc    = table-cell
    t-row          = table-row
    t-columm       = table-column
    t-column-group = table-column-group

   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large

*/

.dn {              display: none; }
.di {              display: inline; }
.db {              display: block; }
.dib {             display: inline-block; }
.dit {             display: inline-table; }
.dt {              display: table; }
.dtc {             display: table-cell; }
.dt-row {          display: table-row; }
.dt-row-group {    display: table-row-group; }
.dt-column {       display: table-column; }
.dt-column-group { display: table-column-group; }

/*
  This will set table to full width and then
  all cells will be equal width
*/
.dt--fixed {
  table-layout: fixed;
  width: 100%;
}
```

`.dn`
```jsx
<div className="dn">This is not visible :)</div>
```

`.di`
```jsx
<div>
  <div className="di bg-white-30">Inline box</div> <div className="di bg-white-50">Inline box</div> <div className="di bg-white-30">Inline box</div>
</div>
```

`.db`
```jsx
<div>
  <span className="db bg-white-30">Block box</span>
  <span className="db bg-white-50">Block box</span>
</div>
```

`.dib`
```jsx
<div>
  <div className="dib bg-white-30">Inline Block box</div> <div className="dib bg-white-50">Inline Block box</div> <div className="dib bg-white-30">Inline Block box</div>
</div>
```

`.td, .tdc`
```jsx
<div className="dt w-100">
  <div className="dtc v-mid pa1 bg-white-30">display</div>
  <div className="dtc v-mid pa1 bg-white-50">table</div>
  <div className="dtc v-mid pa1 bg-white-30">will automatically</div>
  <div className="dtc v-mid pa1 bg-white-50">compute cell width</div>
</div>
```

`.dt--fixed`
```jsx
<div className="dt dt--fixed">
  <div className="dtc v-mid pa1 bg-white-30">display table</div>
  <div className="dtc v-mid pa1 bg-white-50">with table-layout: fixed	</div>
  <div className="dtc v-mid pa1 bg-white-30">will automatically</div>
  <div className="dtc v-mid pa1 bg-white-50">make every cell the same width regardless of the content</div>
</div>
```
