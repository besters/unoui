```css
/*
  BOX SIZING
  Docs: http://tachyons.io/docs/layout/box-sizing/
*/

.border-box {
  box-sizing: border-box;
}
```

`.border-box`
```jsx
<div className="border-box">This has border-box box-sizing</div>
```
