```css
/*
    OVERFLOW

    Media Query Extensions:
      -ns = not-small
      -m  = medium
      -l  = large
 */

.overflow-visible { overflow: visible; }
.overflow-hidden { overflow: hidden; }
.overflow-scroll { overflow: scroll; }
.overflow-auto { overflow: auto; }

.overflow-x-visible { overflow-x: visible; }
.overflow-x-hidden { overflow-x: hidden; }
.overflow-x-scroll { overflow-x: scroll; }
.overflow-x-auto { overflow-x: auto; }

.overflow-y-visible { overflow-y: visible; }
.overflow-y-hidden { overflow-y: hidden; }
.overflow-y-scroll { overflow-y: scroll; }
.overflow-y-auto { overflow-y: auto; }
```

`.overflow-hidden`
```jsx
<div className="w-100 bg-white-10 pv1 measure h4 overflow-hidden">
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce wisi. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam egestas wisi a erat. Duis viverra diam non justo. Aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Donec vitae arcu. Integer pellentesque quam vel velit. In dapibus augue non sapien. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Nunc dapibus tortor vel mi dapibus sollicitudin. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Etiam posuere lacus quis dolor. Nullam dapibus fermentum ipsum. Aliquam erat volutpat. Nunc dapibus tortor vel mi dapibus sollicitudin. Donec vitae arcu.</p>
</div>
```

`.overflow-scroll`
```jsx
<div className="w-100 bg-white-10 pv1 measure h4 overflow-scroll">
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce wisi. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam egestas wisi a erat. Duis viverra diam non justo. Aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Donec vitae arcu. Integer pellentesque quam vel velit. In dapibus augue non sapien. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Nunc dapibus tortor vel mi dapibus sollicitudin. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Etiam posuere lacus quis dolor. Nullam dapibus fermentum ipsum. Aliquam erat volutpat. Nunc dapibus tortor vel mi dapibus sollicitudin. Donec vitae arcu.</p>
</div>
```

`.overflow-auto`
```jsx
<div className="w-100 bg-white-10 pv1 measure h4 overflow-auto">
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce wisi. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam egestas wisi a erat. Duis viverra diam non justo. Aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Donec vitae arcu. Integer pellentesque quam vel velit. In dapibus augue non sapien. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Nunc dapibus tortor vel mi dapibus sollicitudin. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Etiam posuere lacus quis dolor. Nullam dapibus fermentum ipsum. Aliquam erat volutpat. Nunc dapibus tortor vel mi dapibus sollicitudin. Donec vitae arcu.</p>
</div>
```