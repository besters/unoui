```css
/*
  FLEXBOX

  Media Query Extensions:
   -ns = not-small
   -m  = medium
   -l  = large
*/

.flex { display: flex; }
.inline-flex { display: inline-flex; }
.flex-none { flex: none; }

.flex-column  { flex-direction: column; }
.flex-row     { flex-direction: row; }
.flex-wrap    { flex-wrap: wrap; }
.flex-column-reverse  { flex-direction: column-reverse; }
.flex-row-reverse     { flex-direction: row-reverse; }
.flex-wrap-reverse    { flex-wrap: wrap-reverse; }

.items-start    { align-items: flex-start; }
.items-end      { align-items: flex-end; }
.items-center   { align-items: center; }
.items-baseline { align-items: baseline; }
.items-stretch  { align-items: stretch; }

.self-start    { align-self: flex-start; }
.self-end      { align-self: flex-end; }
.self-center   { align-self: center; }
.self-baseline { align-self: baseline; }
.self-stretch  { align-self: stretch; }

.justify-start   { justify-content: flex-start; }
.justify-end     { justify-content: flex-end; }
.justify-center  { justify-content: center; }
.justify-between { justify-content: space-between; }
.justify-around  { justify-content: space-around; }

.content-start   { align-content: flex-start; }
.content-end     { align-content: flex-end; }
.content-center  { align-content: center; }
.content-between { align-content: space-between; }
.content-around  { align-content: space-around; }
.content-stretch { align-content: stretch; }

.order-0 { order: 0; }
.order-1 { order: 1; }
.order-2 { order: 2; }
.order-3 { order: 3; }
.order-4 { order: 4; }
.order-5 { order: 5; }
.order-6 { order: 6; }
.order-7 { order: 7; }
.order-8 { order: 8; }
.order-last { order: 99999; }

.grow-0 { flex-grow: 0; }
.grow-1 { flex-grow: 1; }
.grow-2 { flex-grow: 2; }
.grow-3 { flex-grow: 3; }
.grow-4 { flex-grow: 4; }
.grow-5 { flex-grow: 5; }
.grow-6 { flex-grow: 6; }
.grow-7 { flex-grow: 7; }
.grow-8 { flex-grow: 8; }
.grow-9 { flex-grow: 9; }

.shrink-0 { flex-shrink: 0; }
.shrink-1 { flex-shrink: 1; }
.shrink-2 { flex-shrink: 2; }
.shrink-3 { flex-shrink: 3; }
.shrink-4 { flex-shrink: 4; }
.shrink-5 { flex-shrink: 5; }
.shrink-6 { flex-shrink: 6; }
.shrink-7 { flex-shrink: 7; }
.shrink-8 { flex-shrink: 8; }
.shrink-9 { flex-shrink: 9; }

.flex-distribute { flex: 1 0; }
```

```jsx
<div className="flex justify-center">
  <div className="pa2 bg-white-10 order-1">1</div>
  <div className="pa2 bg-white-10 order-last">2</div>
  <div className="pa2 bg-white-10 order-0">3</div>
  <div className="pa2 bg-white-10 order-1">4</div>
  <div className="pa2 bg-white-10 order-1">5</div>
</div>
```