```css
/*
   WIDTHS
   Docs: http://tachyons.io/docs/layout/widths/

   Base:
     w = width

   Modifiers
     1 = 1st step in width scale
     2 = 2nd step in width scale
     3 = 3rd step in width scale
     4 = 4th step in width scale
     5 = 5th step in width scale

     -10  = literal value 10%
     -20  = literal value 20%
     -25  = literal value 25%
     -30  = literal value 30%
     -33  = literal value 33%
     -34  = literal value 34%
     -40  = literal value 40%
     -50  = literal value 50%
     -60  = literal value 60%
     -70  = literal value 70%
     -75  = literal value 75%
     -80  = literal value 80%
     -90  = literal value 90%
     -100 = literal value 100%

     -third      = 100% / 3 (Not supported in opera mini or IE8)
     -two-thirds = 100% / 1.5 (Not supported in opera mini or IE8)
     -auto       = string value auto


   Media Query Extensions:
     -ns = not-small
     -m  = medium
     -l  = large
*/

/* Width Scale */
.w1 {    width: 1rem; }
.w2 {    width: 2rem; }
.w3 {    width: 4rem; }
.w4 {    width: 8rem; }
.w5 {    width: 16rem; }
.w6 {    width: 18rem; }

/* Literal value */
.w-10 {  width:  10%; }
.w-20 {  width:  20%; }
.w-25 {  width:  25%; }
.w-30 {  width:  30%; }
.w-33 {  width:  33%; }
.w-34 {  width:  34%; }
.w-40 {  width:  40%; }
.w-50 {  width:  50%; }
.w-60 {  width:  60%; }
.w-70 {  width:  70%; }
.w-75 {  width:  75%; }
.w-80 {  width:  80%; }
.w-90 {  width:  90%; }
.w-100 { width: 100%; }

.w-third { width: calc(100% / 3); }
.w-two-thirds { width: calc(100% / 1.5); }
.w-auto { width: auto; }
```

### Width scale
```jsx
<div>
  <div className="w1 bg-white-50 mb2">.w1</div>
  <div className="w2 bg-white-50 mb2">.w2</div>
  <div className="w3 bg-white-50 mb2">.w3</div>
  <div className="w4 bg-white-50 mb2">.w4</div>
  <div className="w5 bg-white-50 mb2">.w5</div>
  <div className="w6 bg-white-50 mb2">.w6</div>
</div>
```

### Width percentages

```jsx
<div className="w-100 bg-white-20">
  <div className="w-10 bg-white-50 mb2">.w-10</div>
  <div className="w-20 bg-white-50 mb2">.w-20</div>
  <div className="w-25 bg-white-50 mb2">.w-25</div>
  <div className="w-30 bg-white-50 mb2">.w-30</div>
  <div className="w-33 bg-white-50 mb2">.w-33</div>
  <div className="w-third bg-white-50 mb2">.w-third</div>
  <div className="w-34 bg-white-50 mb2">.w-34</div>
  <div className="w-40 bg-white-50 mb2">.w-40</div>
  <div className="w-50 bg-white-50 mb2">.w-50</div>
  <div className="w-60 bg-white-50 mb2">.w-60</div>
  <div className="w-two-thirds bg-white-50 mb2">.w-two-thirds</div>
  <div className="w-75 bg-white-50 mb2">.w-75</div>
  <div className="w-80 bg-white-50 mb2">.w-80</div>
  <div className="w-90 bg-white-50 mb2">.w-90</div>
  <div className="w-100 bg-white-50 mb2">.w-100</div>
</div>
```