### Real life example
```jsx
<Sidebar>
  <Menu accordion color="#323237" accentColor="teal-30">
    <Item exact path="/" label="Dashboard" />

    <MenuHeader>CRM</MenuHeader>
    <Item label="Adresář">
      <Item exact path="/crm/companies" label="Společnosti" />
      <Item exact path="/crm/persons" label="Osoby" />
    </Item>
    <Item exact path="/projects" label="Projekty" />
    <Divider />
    <Item exact path="/tickets" label="Tickety" />

    <MenuHeader>Interní</MenuHeader>
    <Item label="Finance">
      <Item exact path="/finances/dashboard" label="Přehled" />
      <Divider />
      <Item label="Vydané Faktury">
        <Item exact path="/finances/issued_invoices" label="Faktury" />
        <Item exact path="/finances/proforma_invoices" label="Zálohové Faktury" />
        <Item exact path="/finances/recurring_invoices" label="Pravidelné Faktury" />
        <Item exact path="/finances/issued_invoices/payments" label="Úhrady" />
      </Item>
      <Item label="Přijaté Faktury">
        <Item exact path="/finances/received_invoices" label="Faktury" icon={<FaGithub size={19} />} />
        <Item exact path="/finances/received_invoices/payments" label="Úhrady" />
      </Item>
      <Item exact path="/finances/bank" label="Banka" />
      <Item exact path="/finances/cash" label="Hotovost" />
    </Item>

    <Divider />

    <Item label="Nastavení">
      <Item exact path="/settings/users" label="Uživatelé" />
      <Item exact path="/settings/permissions" label="Oprávnění" />
    </Item>
  </Menu>
</Sidebar>
```