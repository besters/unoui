# Název aplikace [![build status](https://gitlab.com/unodor/fremundo/badges/master/build.svg)](https://gitlab.com/unodor/fremundo/commits/master)

> Generate a README.md using answers to prompts and data from the environment, like `package.json`, `.git` config, etc. This generator can be run by command line if Generate is installed globally, or you can use this as a plugin or sub-generator in your own generator.

## Table of Contents

- [První instalace](#prvotni-instalace)
- [Aktualizace po stažení posledních změn z GitLabu](#command-line-usage)
- [Interaktivni aktualizace Gemu na posledni verze](#available-tasks)
- [Konfigurace](#running-multiple-generators)
  * [Obecné, necitlivé údaje](#generate-package)
  * [Citlivé údaje](#generate-dest)
- [Developing](#anchor)
- [Deploying](#anchor)
- [Testy](#anchor)
- [Services](#anchor)

## Prepare
```sh
$ brew tap eugenmayer/dockersync
$ brew install eugenmayer/dockersync/unox
$ brew install unison
$ gem install dockrails
```

## První Instalace
> Secrets encryption key je `69ec339881d2de4c8b4023f8f6d156db` (není moc secure, bude vyřešeno později)

```sh
$ bin/setup
```

## Aktualizace po stažení posledních změn z GitLabu

```sh
$ bin/update
```

## Konfigurace
Veškeré config proměnné jsou v aplikaci dostupné přes metodu `Rails.application.secrets.secret_name`

### Obecné, necitlivé údaje
> Necitlivé údaje se nedají zneužít když se k nim dostane někdo nepovolaný. Např. redis_url, různé nastavení aplikace, atp.

Proměnné jsou definovány v `config/secrets.yml`. Protože se hodnoty čtou z ENV proměnných, tak na localhostu musí být definovány v `.env` souborech. V produkci to řeší dokku s `dokku config:set`.

##### Development
Obecné a globální údaje patří do souboru `.env` který se commituje do Gitu a je tak pro všechny vývojaře stejný. Specifické hodnoty, jako např. nestandartní redis adresa a jiné hodnoty, které se liší vývojář od vývojáře patří do `.env.local` který je v .gitignore.

---

##### Production
Moméntalně se nastavují přes `dokku config:set`

### Citlivé údaje
> Citlivé údaje jsou zneužitelné třetí stranou. Platí to hlavně pro různé api klíče a secrety.

##### Development
Patří do `.env.local`, jelikož se tak nedostane do gitu. V žádném případě nesmí jít do `.env`!

---

##### Production
Citlivé údaje se commitují do gitu encryptované. Encription key se nastavoval při instalaci a je uložený v `config/secrets.yml.key`.
Samotný secret file je `config/secrets.yml.enc` a je editovatelný pomocí příkazu

```sh
$ EDITOR="code --wait" bin/rails secrets:edit
```

> EDITOR může být používaný editor. "code" pro Visual studio code, "subl" pro Sublime, "atom" pro Atom...

Na localhostu se `secrets.yml.enc` nepoužívá.



# Konfigurace
Obecné nastavení je v souboru `config/secrets.yml`.
K jednotlivým hodnotám se dá dostat přes `Rails.application.secrets.secret_name`.
Necitlivé údaje se berou z ENV proměnnych, proto v dev prostředí jsou v `.env`, případně `.env.local` souborech a v produkci pomocí `dokku config` příkazů.
Citlivé údaje v dev prostředí patčí zásadně do `.env.local` a pro produkční prostředí pomocí `secrets:edit` (commituje se do gitu)

```sh
$ EDITOR="code --wait" bin/rails secrets:edit
```

kdy EDITOR může být používaný editor. "code" pro Visual studio code, "subl" pro Sublime, "atom" pro Atom...

`.env` se commituje do gitu, proto do něj NEPATŘÍ žádné citlivé údaje! (api klíče, a jiné zneužitelné údaje)
`.env.local` je v .gitignore, veškeré citlivé data tedy patří do něj. Patří zde i lokální změny, které by neměly ovlivnit ostatní vývojáře, např. nestandartní redis port, atp.

## Developing
Vše je integrované do guardu. Musí běžet jenom Postgres satabáze a pokud je to releventní, tak i Redis a poté stačí už jenom spustit Guard příkazem

	$ bin/guard

jít na [http://localhost:3000](http://localhost:3000) a vesele vyvíjet :)
> Gemy by se měly instalovat automaticky, server by se měl sám restartovat když je to potřeba. Testy se taky spouští samy.

## Deploying

## Testy

## Services (job queues, cache servers, search engines, etc.)

## Javascript
JavaScripty patří do `/client` složky obsluhované WebPackem. Do Assetů nic nepatří pokud to není 100% nezbytně nutné.

## CSS

## Secure headers

## Analytics a jine trackery
Je použitý [rack-tracker](https://github.com/railslove/rack-tracker) gem. Nastavení trackerů je v controllerech v before action metodě `set_trackers`.

## Interaktivni aktualizace Gemu na posledni verze
	$ bin/bundleup

## Kubernetes (TODO)
    kubeclt apply secrets generic prod-secrets --from-file=.env