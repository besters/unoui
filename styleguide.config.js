const { join } = require('path')
const webpack = require('./config/webpack.config.dev')
const glob = require('glob')
const _ = require('lodash')

const ignore = ['./src/components/card/Header.jsx']

const allExcept = (pattern = '*') => {
  const folders = pattern == '*' ? pattern : `!(${pattern.join('|')})`
  const files = glob.sync(`./src/components/${folders}/*.jsx`)

  // remove ignored paths
  _.remove(files, path => _.includes(ignore, path))

  return files
}

const layoutComponents = () => {
  return [
    './src/components/layout/Layout.jsx',
    './src/components/page/Page.jsx',
    './src/components/navbar/Navbar.jsx',
    './src/components/sidebar/Sidebar.jsx',
    './src/components/sidebar-control/SidebarControl.jsx',
  ]
}

const navigationComponents = () => {
  return [
    './src/components/vertical-menu/Menu.jsx',
    './src/components/vertical-menu/Item.jsx',
    './src/components/vertical-menu/MenuHeader.jsx',
    './src/components/vertical-menu/Divider.jsx',
  ]
}

module.exports = {
  webpackConfig: webpack,
  require: [join(__dirname, './src/index.scss')],
  context: {
    Swatch: join(__dirname, './src/styleguide/Swatch.jsx'),
    FaCogs: 'react-icons/lib/fa/cogs',
    FaGithub: 'react-icons/lib/fa/github',
    IoIosGearOutline: 'react-icons/lib/io/ios-gear-outline',
  },
  styleguideComponents: {
    Wrapper: join(__dirname, './src/styleguide/Wrapper.jsx'),
  },
  showUsage: true,
  title: 'UnoUI',
  sections: [
    {
      name: 'Introduction',
      content: 'docs/introduction.md',
    },
    {
      name: 'Documentation',
      sections: [
        {
          name: 'Installation',
          content: 'docs/installation.md',
        },
        {
          name: 'Configuration',
          content: 'docs/configuration.md',
        },
      ],
    },
    {
      name: 'UI Components',
      components: () =>
        allExcept([
          'layout',
          'page',
          'navbar',
          'sidebar',
          'sidebar-control',
          'vertical-menu',
        ]),
      sections: [
        {
          name: 'Layout',
          components: () => layoutComponents(),
          content: 'docs/layout.md',
        },
        {
          name: 'Navigation',
          components: () => navigationComponents(),
          content: 'docs/navigation.md',
        },
      ],
    },
    {
      name: 'Appearance',
      content: 'docs/appearance.md',
      sections: [
        {
          name: 'Typography',
          sections: [
            {
              name: 'Font Families',
              content: 'docs/appearance/typography/font-families.md',
            },
            {
              name: 'Type Scale',
              content: 'docs/appearance/typography/scale.md',
            },
            {
              name: 'Measure',
              content: 'docs/appearance/typography/measure.md',
            },
            {
              name: 'Line Height',
              content: 'docs/appearance/typography/line-height.md',
            },
            {
              name: 'Letter Spacing',
              content: 'docs/appearance/typography/tracking.md',
            },
            {
              name: 'Font Weight',
              content: 'docs/appearance/typography/font-weight.md',
            },
            {
              name: 'Font Style',
              content: 'docs/appearance/typography/font-style.md',
            },
            {
              name: 'Vertical Align',
              content: 'docs/appearance/typography/vertical-align.md',
            },
            {
              name: 'Text Align',
              content: 'docs/appearance/typography/text-align.md',
            },
            {
              name: 'Text Transform',
              content: 'docs/appearance/typography/text-transform.md',
            },
            {
              name: 'Text Decoration',
              content: 'docs/appearance/typography/text-decoration.md',
            },
            {
              name: 'White Space',
              content: 'docs/appearance/typography/white-space.md',
            },
          ],
        },
        {
          name: 'Layout',
          sections: [
            {
              name: 'Box Sizing',
              content: 'docs/appearance/layout/box-sizing.md',
            },
            {
              name: 'Spacing',
              content: 'docs/appearance/layout/spacing.md',
            },
            {
              name: 'Negative Margins',
              content: 'docs/appearance/layout/negative-margins.md',
            },
            {
              name: 'Floats',
              content: 'docs/appearance/layout/floats.md',
            },
            {
              name: 'Clears',
              content: 'docs/appearance/layout/clears.md',
            },
            {
              name: 'Display',
              content: 'docs/appearance/layout/display.md',
            },
            {
              name: 'Widths',
              content: 'docs/appearance/layout/widths.md',
            },
            {
              name: 'Max Widths',
              content: 'docs/appearance/layout/max-widths.md',
            },
            {
              name: 'Heights',
              content: 'docs/appearance/layout/heights.md',
            },
            {
              name: 'Position',
              content: 'docs/appearance/layout/position.md',
            },
            {
              name: 'Flexbox',
              content: 'docs/appearance/layout/flexbox.md',
            },
            {
              name: 'Aspect Ratios',
              content: 'docs/appearance/layout/aspect-ratios.md',
            },
            {
              name: 'Overflow',
              content: 'docs/appearance/layout/overflow.md',
            },
            {
              name: 'Word Break',
              content: 'docs/appearance/layout/word-break.md',
            },
            {
              name: 'Utilities',
              content: 'docs/appearance/layout/utilities.md',
            },
          ],
        },
        {
          name: 'Theming',
          sections: [
            {
              name: 'Colors',
              content: 'docs/appearance/theming/colors.md',
            },
            {
              name: 'Color Usage',
              content: 'docs/appearance/theming/color-usage.md',
            },
            {
              name: 'Box Shadows',
              content: 'docs/appearance/theming/box-shadows.md',
            },
            {
              name: 'Outlines',
              content: 'docs/appearance/theming/outlines.md',
            },
            {
              name: 'Rotations',
              content: 'docs/appearance/theming/rotations.md',
            },
            {
              name: 'Hover Effects',
              content: 'docs/appearance/theming/hovers.md',
            },
            {
              name: 'Background Size',
              content: 'docs/appearance/theming/background-size.md',
            },
            {
              name: 'Background Position',
              content: 'docs/appearance/theming/background-position.md',
            },
            {
              name: 'Borders',
              content: 'docs/appearance/theming/borders.md',
            },
            {
              name: 'Border Radius',
              content: 'docs/appearance/theming/border-radius.md',
            },
            {
              name: 'Z-index',
              content: 'docs/appearance/theming/z-index.md',
            },
            {
              name: 'Opacity',
              content: 'docs/appearance/theming/opacity.md',
            },
            {
              name: 'Visibility',
              content: 'docs/appearance/theming/visibility.md',
            },
          ],
        },
      ],
    },
  ],
}
